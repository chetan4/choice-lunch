<?php get_header(); ?>

    <div id="main">

      <section>
        <div class="container">
          <div class="row">
            <div class="span8 content-area">
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="row post">
                <div class="span1 date hidden-phone">
                  <div class="month"><?php the_time( 'M' ); ?></div>
                  <div class="day"><?php the_time( 'j' ); ?></div>
                  <div class="year"><?php the_time( 'Y' ); ?></div>
                </div>
                <div class="span7">
                  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                  <div class="meta">By <?php the_author_posts_link() ?><span class="hidden-desktop hidden-tablet"> on <?php the_time( 'M j, Y' ); ?></span></div>
                  <div class="entry">
                    <?php if ( has_post_thumbnail() ) : ?>
                    <?php $src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>
                    <a class="alignright popup" href="<?php echo $src; ?>" rel="attachment">
                      <img class="round-border pull-right" style="width: 200px;" src="<?php echo $src; ?>" alt="<?php the_title_attribute(); ?>" title="<?php the_title_attribute(); ?>" /></a>
                    </a>
                    <?php endif; ?>
                    <?php the_content(); ?>
                  </div>
                  <div class="meta">Posted in <?php the_category(', '); ?></div>
                  <span class='st_facebook_hcount' displayText='Facebook'></span>
                  <span class='st_twitter_hcount' displayText='Tweet'></span>
                  <span class='st_email_hcount' displayText='Email'></span>
                  <?php comments_template( '', true ); ?>
                </div>
              </div>
              <?php endwhile; else: ?>
              <div class="alert alert-error">
                <h4>Oh Snap!</h4>
                <p>There aren't any blog posts</p>
              </div>
              <?php endif; ?>
            </div>
            <div class="span4 sidebar hidden-phone">
              <div class="row">
                <div class="span4">
                  <?php get_sidebar(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>