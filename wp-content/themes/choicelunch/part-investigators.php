<?php

query_posts( array( 'post_type' => 'investigator', 'post_status' => 'publish', 'posts_per_page' => '-1', 'suppress_filters' => 0 ) );
if ( have_posts() ) {
	echo '<div class="hidden-phone">';
		echo '<ul class="unstyled investigators">';
		while ( have_posts() ) {
		  the_post();
		  $custom = get_post_custom( $post->ID );
		  // $photo = $custom["investigator_photo"][0];
		  $src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		  echo '<li><a class="popup-investigator photo small" href="' . get_permalink() . '" onclick="_gaq.push([\'_trackEvent\', \'Ingredient Investigators\', \'Image Click\', \'' . get_the_title() . '\']);"><img src="' . get_bloginfo( 'template_directory' ) . '/mk_thumb.php?src=' . $src . '&h=80&w=60' . '" alt="' . get_the_title() . '" title="' . get_the_title() . '" /></a></li>';
		}
		echo '</ul>';
	echo '</div>';
	echo '<div class="hidden-tablet hidden-desktop">';
		echo '<div class="unstyled investigators">';
		while ( have_posts() ) {
		  the_post();
		  $custom = get_post_custom( $post->ID );
		  $src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
		  echo '<div class="row">';
		  echo '<img class="photo" src="' . $src . '" alt="" />';
		  echo '<h4>' . get_the_title() . '</h4>';
          echo '<div class="entry">';
          the_content();
          echo '</div>';
		  echo '</div>';
		}
		echo '</div>';
	echo '</div>';
}
else {
	// output nothing
}
wp_reset_query();