<?php if ( ! isset( $_GET['content_only'] ) && ! $_GET['content_only'] == 'true' ) : ?>
<?php get_header(); ?>

    <div id="main">

      <section>
        <div class="container">
          <div class="row">
            <div class="span12 content-area">
              <div class="row post">
                <div class="span12">
<?php endif; ?>
                  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                  <div id="investigator_card">
                    <div class="aside">
                      <?php $src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?><img class="round-border" src="<?php echo $src; ?>" alt="" />
                    </div>
                    <div class="detail">
                      <h2><?php the_title(); ?></h2>
                      <div class="entry">
                        <?php the_content(); ?>
                      </div>
                    </div>
                  </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
<?php if ( ! isset( $_GET['content_only'] ) && ! $_GET['content_only'] == 'true' ) : ?>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>
<?php endif; ?>