<?php
/*
Template Name: Order Now
*/
?>
<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span6 offset3 intro">
              <?php
                $pid = ($post->post_parent?$post->post_parent:$post->ID);
                $page = get_page($pid);
              ?>
              <h1 class="talignleft"><?php echo ( function_exists('the_subheading') && get_the_subheading($pid) != '' ? get_the_subheading($pid) : get_the_title($pid) ); ?></h1>
              <?php echo $page->post_content; ?>
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>
      </section>
	
	<noscript>
		<style type="text/css">
			#login {visibility:hidden; display:none;}
		</style>
		<div class="row">
		  <div class="span6 offset4">
			<br /><br />
			<div align="center"><asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ChoiceLunchLogo_300x53.gif" alt="Choicelunch" style="margin-bottom: 20px;" /></div>
			<div class="alert alert-error" id="divErrorMessage">
				<h4 class="alert-heading"><i class="icon-warning-sign"></i> Javascript Required for Choicelunch</h4>
				<p>It looks like your browser has JavaScript turned off or disabled.  This means you won't be able to use the Choicelunch website since
				Choicelunch.com has a lot of cool stuff that depends on Javascript.</p>

				<p>The good news is that it's easy to turn JavaScript on and get on with your Choicelunch-ing.  Here's how for a few common browsers:</p>

				<ul>
					<li><b>Internet Explorer</b>
						<ol>
							<li>From the Tools menu, or the Tools drop-down in the upper right, choose Internet options.</li>
							<li>Click the Security tab.</li>
							<li>Click Custom Level... . </li>
							<li>Scroll to the "Scripting" section of the list. For "Active Scripting", click Disable or Enable.</li>
							<li>Click OK, and confirm if prompted.</li>
							<li>Close and restart your browser.</li>
						</ol>
						<br />
					</li>
					<li><b>Safari</b>
						<ol>
							<li>Open Safari Preferences</li>
							<li>Click on "Advanced" and check the box next to "Show Develop menu in menu bar"</li>
							<li>Pull down the "Develop" menu and select "Disable Javascript", a check signifies it’s disabled</li>
						</ol>
						<br />
					</li>
					<li><b>Chrome</b>
						<ol>
							<li>Open Google Chrome's Preferences (click wrench icon top-right)</li>
							<li>Chose "Settings..."</li>
							<li>Click the "Show advanced settings..." link at the bottom</li>
							<li>Privacy -> Content Settings...</li>
							<li>Check Javascript -> "Allow all sites to run JavaScript (recommended)"</li>
							<li>Ok, close Settings tab</li>
						</ol>
						<br />
					</li>
					<li><b>Firefox</b>
						<ol>
							<li>Open Firefox's menu (click on 'Firefox' top-left)</li>
							<li>Chose "Options > Options"</li>
							<li>Click the "Content" tab</li>
							<li>Check "Enable Javascript" checkbox</li>
							<li>Ok</li>
						</ol>
					</li>
				</ul>

			</div>
		  </div>
		</div>
	</noscript>
	
    <div class="ltIE7">
        <div class="row">
        <div class="span6 offset4">
        <br /><br />
        <div align="center"><img src="https://order.choicelunch.com/Images/ChoiceLunchLogo_300x53.gif" style="margin-bottom: 20px;" /></div>
        <div class="alert alert-error">
            <h4 class="alert-heading"><i class="icon-warning-sign"></i> Browser Not Supported :(</h4>
            <p>www.Choicelunch.com requires use of Internet Explorer 7 or higher and <b>your browser does not meet this requirement</b>.</p>

            <p>A newer version of your chosen browser is necessary as www.Choicelunch.com has been updated with new features and a great
            new experience which takes advantage of technology in browsers released after 2006.</p>

            <p>The good news is that's it's a free and easy process to update your browser to the 
            <a style="color:blue;" href="http://go.microsoft.com/fwlink/?LinkId=158420">latest Internet Explorer</a>
            and <i>all your websites</i> will run faster and have more features.</p>

            <p>If you are unable to update your Internet Explorer browser or use a different browser, you may always call Customer Service and we'll help get your
            lunch order placed:  (855) GO-LUNCH.</p>

        </div>
        </div>
        </div>
    </div>

    <div class="ltIE8">
        <div class="row">
        <div class="span6 offset4">
        <br /><br />
        <div class="alert alert-warning">
            <h4 class="alert-heading"><i class="icon-warning-sign"></i> Browser Not Optimal</h4>
            <p>www.Choicelunch.com is designed to work with Internet Explorer 8 or higher. 
            While you may try using www.Choicelunch.com using your current IE 7 browser, <b>your experience will be slower</b>.</p>

            <p>The good news is that's it's a free and easy process to update your browser to the 
            <a style="color:blue;" href="http://go.microsoft.com/fwlink/?LinkId=158420">latest Internet Explorer</a>
            and <i>all</i> your websites will run faster and have more features.</p>

            <p><i style="color:#999;font-size:8pt;">Note that this message may also appear if you are using IE 8 or IE 9 in "Compatability Mode".
            This mode is not the default and may cause this website and others to display incorrectly.<br />
            To turn this off, click on the 'ripped page' icon in your address bar or Tools -> (uncheck) Compatability Mode.</i>
            <br />
            <div align="center"><img src="https://order.choicelunch.com/Images/IECompatMode.png" Width="501" Height="65" alt="Turn off Compatibility Moode" style="margin-top: 10px;" /></div>
            </p>
        </div>
        </div>
        </div>
    </div>
	
      <!-- Sign In Options -->
      <section id="signin_options">
        <div class="container">
          <div class="row">
            <div class="span6 offset3 option">
              <h2>Have a School Code? Enter&nbsp;It&nbsp;Now:</h2>
              <div class="row">
                <div class="span3 form">
                  <form class="form-vertical" action="https://order.choicelunch.com/Account/Registration/AccountSetup.aspx" method="post">
                    <input id="remote" name="remote" type="hidden" value="REMOTE" />
                    <div class="control-group">
                        <label class="control-label">School Code:</label>
                        <div class="control">
                            <input type="text" class="input" name="registrationcode" style="width: 70%;" placeholder="" />
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="control">
                            <button type="submit" class="btn btn-small btn-orange" name="btnSignUp" onclick="_gaq.push(['_trackEvent', 'Order Now Page', 'Sign Up Click', 'XYZ']);">Sign up</button>
                        </div>
                    </div>
                    <a class="popup" href="#nocode" onclick="_gaq.push(['_trackEvent', 'Order Now Page', 'Dont have a code? Link Click']);">Don't have a code?</a>
                    <div id="nocode" style="display:none;width:400px"><h3>Don't have a school code?</h3><p>If you’re wondering how to access the site, or if your school has Choicelunch but you can’t remember the code, you’ve come to the right place. <a href="mailto:customerservice@choicelunch.com">Contact us</a> and a Choicelunch representative will get back to you in the time it takes to whip up some pureed veggies.</p></div>
                  </form>
                </div>
                <div class="span3 screenshot">
                  <img src="<?php bloginfo( 'template_directory' ); ?>/img/order-now/school_code_example.png" alt="" title="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

<?php get_footer(); ?>