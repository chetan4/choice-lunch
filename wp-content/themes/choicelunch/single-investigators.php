<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span8 offset2 intro">
              <h1>Real Food. Real Chefs. Real Choices.</h1>
              <p>Real food is picking fruits from the tree and veggies off the vine. It’s making sure each meal is packed with as many nutrients as we can fit. It’s vegetarian-fed cows and healthy chickens growing up happy as clams. It’s no additives, preservatives, hormones, or creepy scientists in white lab coats. It’s real food, prepared by real people with direct input from our realest of clients: Our kids.</p>
            </div>
          </div>
        </div>
      </section>

      <section id="ourfood_tabs">
        <div class="container">
          <div class="row">
            <div class="span12">
              <ul class="tabs">
                <li><a href="/food/ingredients/">Our Ingredients</a></li>
                <li><a href="/food/process/">Our Process</a></li>
                <li><a href="/food/chefs/">Our Chefs</a></li>
                <li><a href="/food/values/">Our Values</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <section id="ourfood_tab_content">
        <div class="container">
          <div class="row">
            <div class="span12">
              <div class="row">
                <div class="span8">
                  <p>In a word, real. We don’t believe in adding anything artificial to our food because real— when done right — is not just healthier, it tastes better. Our meat, poultry and dairy come from suppliers that don’t believe in hormones or antibiotics. There’s no trans fat. No MSG. No high fructose corn syrup. And no artificial colors, flavors or sweeteners.</p>
                  <p>Our fruits and veggies are largely sourced from local farmers who share our commitment to fresh, unaltered produce. As simple as it may seem, pretty much everything we need to make a nutritious lunch can be found right in our own sun-drenched, California backyard. So whether it’s slow roasting a turkey in our kitchen or making Pico de Gallo from scratch, these are the ingredients we live by.</p>
                  <h3>Meet the Investigators</h3>
                  <p>photos</p>
                </div>
                <div class="span4">right</div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>