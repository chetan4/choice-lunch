<?php
/*
Template Name: Get Choicelunch
*/
?>
<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span8 offset2 intro">
              <?php
                $pid = ($post->post_parent?$post->post_parent:$post->ID);
                $page = get_page($pid);
              ?>
              <h1><?php echo ( function_exists('the_subheading') && get_the_subheading($pid) != '' ? get_the_subheading($pid) : get_the_title($pid) ); ?></h1>
              <?php echo $page->post_content; ?>
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>
      </section>

      <!-- Contact Options -->
      <section id="contact_options">
        <div class="container">
          <div class="row equal-heights">
            <div class="span4 option">
              <div class="detail">
                <p>If you have kids who like food, we’d love to hear from you.</p>
              </div>
              <div class="icon">
                <p><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_contact_choicelunch.png" alt="" title="" /></p>
              </div>
              <a class="btn btn-block btn-large btn-orange" href="<?php echo get_page_link(20); ?>" onclick="_gaq.push(['_trackEvent', 'Contact Us Link', 'Contact Choicelunch Button Click']);">Contact Choicelunch</a>
            </div>
            <div class="span4 option">
              <div class="detail">
                <p>Want more? Here you will find facts, figures and lots of pretty pictures.</p>
              </div>
              <div class="icon">
                <p><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_download_pdf.png" alt="" title="" /></p>
              </div>
              <a class="btn btn-block btn-large btn-orange" href="/assets/InfoPak.pdf" onclick="_gaq.push(['_trackEvent', 'Downloads', 'Download an Info Pack Button', 'InfoPak.pdf']);">Download an Info Pack</a>
            </div>
            <div class="span4 option">
              <div class="detail">
                <p>When it comes to healthy lunches for kids, the more mouths the merrier.</p>
              </div>
              <div class="icon">
                <p><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_friends.png" alt="" title="" /></p>
              </div>
              <a class="btn btn-block btn-large btn-orange" href="mailto:?subject=<?php echo "Check out Choicelunch!"; ?>&body=<?php echo "I thought you may be interested in learning about Choicelunch - it's a California-based school lunch company committed to giving kids real, healthy fresh lunches. Might be a great fit for our school! \n\nRead more about it at choicelunch.com"; ?>" onclick="_gaq.push(['_trackEvent', 'Tell a Friend', 'Tell a Friend Button Click', '?subject=Check out ...']);"> Tell a Friend</a>
            </div>
          </div>
        </div>
      </section>

      <!-- General -->
      <section id="about_extras">
        <div class="container">
          <div class="row">
            <div class="span4">
              <h3>Parents</h3>
              <p>We’re parents ourselves so we understand how important it is that our food lives up to the highest nutritional standards. The Choicelunch promise means you can be sure:</p>
              <ul>
                <li>Your child will eat a healthy lunch every day.</li>
                <li>Whenever possible, lunch will be sourced from local ingredients.</li>
                <li>No additives, preservatives, or artificial colors or sweeteners will mysteriously find their way into the food.</li>
                <li>Our recipes feature seasonal fruits and veggies, antibiotic-free beef and poultry, whole grain breads and homemade sauces and dressings.</li>
                <li>Barring an apocalyptic tornado, or a Giants World Series win, lunch will always be delivered on time.</li>
                <li>All nutritional and allergen information is easy to find.</li>
                <li>Your child will develop healthy eating habits and have an active voice in our process.</li>
              </ul>
            </div>
            <div class="span4">
              <h3>Schools</h3>
              <p>There’s a reason why so many schools across California have joined the Choicelunch family. Here’s just a few:</p>
              <ul>
                <li>16 daily entrees that empower kids to make healthy choices.</li>
                <li>Eco-friendly lunch containers and fruit bags are biodegradable and made from renewable resources.</li>
                <li>Satellite-tracked delivery trucks and temperature monitoring system assures lunches arrive fresh and on time.</li>
                <li>Choicelunch collaborates with the Bay Area Green Business Program and is a Certified Green Business.</li>
              </ul>
           </div>
            <div class="span4">
              <h3>Preschools &amp; Camps</h3>
              <p>Hey, campers and preschoolers eat too! Choicelunch and its many delicious options are also available for your preschoolers and summer campers. Contact us to see how we can deliver our signature nutritious lunches to your kids, wherever they may be.</p>
              <p><a class="btn btn-large btn-block btn-green" href="/contact" onclick="_gaq.push(['_trackEvent', 'Contact Us Link', 'Contact Us Button Click']);"><i class="icon-envelope-alt"></i> Contact Us</a></p>
            </div>
          </div>
        </div>
      </section>

      <!-- Photos -->
      <section id="team">
        <div class="container">
          <div class="row">
            <div class="photo_stack">
              <div class="photo landscape"><img src="<?php bloginfo( 'template_directory' ); ?>/img/get-choicelunch/01-getchoice-food.jpg" alt="" title="" /></div>
              <div class="photo landscape"><img src="<?php bloginfo( 'template_directory' ); ?>/img/get-choicelunch/02-getchoice-kids.jpg" alt="" title="" /></div>
              <div class="photo landscape"><img src="<?php bloginfo( 'template_directory' ); ?>/img/get-choicelunch/03-getchoice-truck.jpg" alt="" title="" /></div>
              <div class="photo landscape"><img src="<?php bloginfo( 'template_directory' ); ?>/img/get-choicelunch/04-getchoice-staff.jpg" alt="" title="" /></div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>