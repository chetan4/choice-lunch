<?php get_header(); ?>

    <div id="main">

      <section>
        <div class="container">
          <div class="row">
            <div class="span12 content-area">
              <div class="row">
                <div class="span12" style="margin-bottom: 20px; text-align: center;">
                  <h1 style="margin-bottom: 40px;">Search results for: '<?php echo ( isset( $_GET['s'] ) ? $_GET['s'] : '' ); ?>'</h1>
                  <form class="form-search" role="search" id="searchform" action="<?php echo home_url( '/' ); ?>" method="get">
                    <input class="input search-query span8" type="text" name="s" placeholder="" id="s" value="<?php echo ( isset( $_GET['s'] ) ? $_GET['s'] : '' ); ?>" />
                    <input class="btn btn-orange" type="submit" id="searchsubmit" value="Search" />
                  </form>
                </div>
              </div>

              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="row post">
                <div class="span12">
                  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                  <div class="meta">By <?php the_author_posts_link() ?> on <?php the_time( 'M j Y' ); ?></div>
                  <div class="row">
                    <div class="span12">
                      <div class="entry">
                        <?php the_excerpt(); ?>
                      </div>
                      <div class="meta">Posted in <?php the_category(', '); ?> |  <a href="<?php the_permalink(); ?>#comments"><?php comments_number( 'no comments yet', '1 comment', '% responses' ); ?></a></div>
                    </div>
                  </div>
                </div>
              </div>
              <?php endwhile; else: ?>
              <div class="alert alert-error">
                <h4>Oh Snap!</h4>
                <p>Your search for '<?php echo ( isset( $_GET['s'] ) ? $_GET['s'] : '' ); ?>' didn't match any posts or pages.</p>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </section>
    </div>

<?php get_footer(); ?>