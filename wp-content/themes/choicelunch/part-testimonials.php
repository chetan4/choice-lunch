<?php query_posts( array( 'post_type' => 'testimonial', 'post_status' => 'publish', 'posts_per_page' => '-1', 'suppress_filters' => 0 ) ); ?>
<?php if ( have_posts() ) : ?>

	<div class="slider">
		<?php while ( have_posts() ) : the_post(); ?>
		<div class="row slide testimonial">
			<div class="span12"><div class="inside"><?php the_content(); ?></div></div>
		</div>
		<?php endwhile; ?>
	</div>
<?php else : ?>
	<?php // output nothing ?>
<?php endif; ?>
<?php wp_reset_query(); ?>