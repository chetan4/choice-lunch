<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span10 offset1 intro">
              <h1 class="expandable"><a class="expander" href="#section-we" onclick="_gaq.push(['_trackEvent', 'Inline Link', 'Link Click', 'We']);">We</a> <span id="section-we" style="display: none;">are dedicated to providing kids with better. Better ingredients. Better entrees. Better futures. From locally sourced fruits and veggies to our one-of-a-kind homemade bagel dog, we</span> make scrumptious, nutritious <a class="expander" href="#section-lunches" onclick="_gaq.push(['_trackEvent', 'Inline Link', 'Link Click', 'lunches']);">lunches</a> <span id="section-lunches" style="display: none;">. Because if we truly are what we eat, who chooses to be saturated, artificial or preserved? Parents want real foods</span> that lead to <a class="expander" href="#section-healthier" onclick="_gaq.push(['_trackEvent', 'Inline Link', 'Link Click', 'healthier']);">healthier</a> lives<span id="section-healthier" style="display: none;">, longer attention spans and bigger brains</span>.</h1>
            </div>
          </div>
          <div class="row">
            <div class="span10 offset1 intro-video" style="text-align: center;">
              <iframe id="v1" src="http://player.vimeo.com/video/48617000?api=1&player_id=v1" style="margin-right:-1px;margin-left:-1px;margin-top:-1px;padding-right:1px;background:#fff;-webkit-box-sizing: border-box; -moz-box-sizing: padding-box; box-sizing: padding-box;display:block;border: 0;overflow:hidden;outline: none;" width="500" height="281" frameborder="0" allowtransparency="true" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
            </div>
          </div>
        </div>
      </section>

      <!-- How it works -->
      <section id="how" class="emphasis">
        <div class="container">
          <div class="row">
            <div class="span12">
              <h2>How it Works</h2>
            </div>
          </div>
          <div class="row">
            <div class="span3 step">
              <h3><a href="https://order.choicelunch.com/Account/Registration/RegistrationCode.aspx" onclick="_gaq.push(['_trackEvent', 'How It Works Section', 'Link Click', 'Sign Up']);" style="color:inherit;text-decoration:inherit;"><span class="number">1</span> Sign Up</a></h3>
              <div class="detail">
                <p>For new users, simply enter your school code to access our menu of healthy options. </p>
              </div>
            </div>
            <div class="span3 step">
              <h3><span class="number">2</span> Order Lunches</h3>
              <div class="detail">
                <p>Our online ordering system and mobile App make it ridiculously easy for parents to order school lunches. </p>
              </div>
           </div>
            <div class="span3 step">
              <h3><span class="number">3</span> Bon App&eacute;tit</h3>
              <div class="detail">
                <p>Incredible entrees. Salivating sides. Fresh fruits and veggies. The best part about Choicelunch, is eating it.</p>
              </div>
            </div>
            <div class="span3 callout">
              <p class="hidden-phone"><a class="btn btn-large btn-orange btn-block" href="https://order.choicelunch.com" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Sign In / Order']);">Sign In / Order</a></p>
              <p class="hidden-desktop hidden-tablet"><a class="btn btn-large btn-orange btn-block" href="https://order.choicelunch.com" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Sign In / Order']);">Sign In / Order</a></p>
              <p class="hidden-phone"><a class="btn btn-green btn-block" href="http://order.choicelunch.com/demo" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Try a Live Demo']);">Try a Live Demo</a></p>
              <h3>Taste the Awesome<sup>&reg;</sup></h3>
              <p><a href="https://order.choicelunch.com/Account/Registration/RegistrationCode.aspx" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Link Click', 'Find out how...']);">Find out how you can bring Choicelunch to your school, pre-school or summer camp.</a></p>
            </div>
          </div>
        </div>
      </section>

      <!-- Social -->
      <section id="social">
        <div class="container">
          <div class="row">
            <div class="span4 select twitter">
              <p>Just sat down w/The Child to select first few weeks of hot lunch. Thx @choicelunch for making it easy!! #backtoschool</p>
              <cite>@OmnivorousMom</cite>
            </div>
            <div class="span4 select facebook">
              <p>My son loves your lunches. I skipped a week and he complained every day that week. Thank you for making lunch easy.</p>
              <cite>&mdash; Kerry Babbeh</cite>
           </div>
            <div class="span4 select message">
              <p>My boys, in 1st and 3rd grade, are fussy eaters and love getting Choicelunch…this makes it much easier to get them ready for school every day!</p>
              <cite>&mdash; Mom, Mill Valley</cite>
            </div>
          </div>
        </div>
      </section>


      <!-- Team -->
      <section id="team">
        <div class="container">
          <div class="row">
            <div class="span12">
              <h2>Smart Kids. Healthy Choices.</h2>
              <p><a href="<?php echo get_page_link(11); ?>" onclick="_gaq.push(['_trackEvent', 'Inline Link', 'Link Click', 'Learn about the team at Choicelunch.']);">Learn about the team at Choicelunch.</a></p>
            </div>
          </div>
          <div class="row">
            <div class="photo_stack">
              <div class="photo"><img src="<?php bloginfo( 'template_directory' ); ?>/img/homepage/01-homepage-produce.jpg" alt="" title="" /></div>
              <div class="photo"><img src="<?php bloginfo( 'template_directory' ); ?>/img/homepage/02-homepage-market.jpg" alt="" title="" /></div>
              <div class="photo"><img src="<?php bloginfo( 'template_directory' ); ?>/img/homepage/03-homepage-prep.jpg" alt="" title="" /></div>
              <div class="photo"><img src="<?php bloginfo( 'template_directory' ); ?>/img/homepage/04-homepage-kid.jpg" alt="" title="" /></div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>