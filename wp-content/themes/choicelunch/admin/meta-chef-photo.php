<div style="display: block; padding-bottom: 20px;">
	<div class="field">
		<label>Attach a photo of the chef</label>
		<input style="width: 60%" type="text" name="chef_photo" id="chef_photo" value="<?php echo (isset($chef_photo) ? $chef_photo : ''); ?>" />
		<a style="margin-left: 5px;" class="button btn_chef_photo" id="chef_photo" href="#">Attach</a>		
	</div>
	<div class="field img_preview">
	</div>
	
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory') . '/admin/style-admin.css'; ?>">

	<script>
		jQuery(function() {
			var formfield=null;
			var formcaption=null;
			window.original_send_to_editor = window.send_to_editor;
			window.send_to_editor = function(html){
				if (formfield) {
					var fileurl = jQuery('img',html).attr('src');
					// alert( fileurl );
					formfield.val(fileurl);
					tb_remove();
				} else {
					window.original_send_to_editor(html);
				}
				formfield=null;
				update_image();
			};
		 
			jQuery('.btn_chef_photo').live('click', function() {
		 		formfield = jQuery( 'input#chef_photo' );
		 		tb_show('', 'media-upload.php?type=image&TB_iframe=true');
				jQuery('#TB_overlay,#TB_closeWindowButton').bind("click",function(){ formfield=null; });
				return false;
			});
			
			jQuery(document).keyup(function(e) {
		  		if (e.keyCode == 27) formfield=null;
			});

			jQuery( 'input#chef_photo' ).blur( function() {
				update_image();
			})

			jQuery( '.photo_del' ).live( 'click', function(e) {
				jQuery( 'input#chef_photo' ).val( '' );
				update_image();
				e.preventDefault();
			});

			update_image();
		});

		function update_image() {
			var img_field_src = jQuery( 'input#chef_photo' ).val();
			var img_preview = jQuery( 'div.img_preview' );
			
			if( img_field_src != '' ) {
				if ( img_preview.attr( 'src' ) != img_field_src ) {
					jQuery( img_preview ).html( '<label>Preview</label><img src="' + img_field_src + '" /><br /><a class="button photo_del" href="#">Delete photo</a>' );
				}
			}
			else {
				jQuery( img_preview ).html( '' );
			}
		}
	</script>
	
	<div class="clear"></div>
</div>