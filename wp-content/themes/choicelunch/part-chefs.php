<?php query_posts( array( 'post_type' => 'chef', 'post_status' => 'publish', 'posts_per_page' => '-1', 'suppress_filters' => 0 ) ); ?>
<?php if ( have_posts() ) : ?>

	<div class="slider">
		<?php while ( have_posts() ) :
		  the_post();
		  $custom = get_post_custom( $post->ID );
		  $photo = $custom["chef_photo"][0];
		?>
		<div class="row slide">
			<div class="span2 offset2" style="text-align: center;"><?php echo ( $photo != '' ? '<img class="photo" src="'.$photo.'" />' : '<img class="photo" style="width: 90%" src="' . get_bloginfo('template_directory') . '/img/missing-food-image.png" />' ); ?><h4 class="chef_name"><?php the_title(); ?></h4 style="margin-top: 20px;"></div>
			<div class="span6">
				<?php the_content(); ?>
			</div>
			<div class="span2">&nbsp;</div>
		</div>
		<?php endwhile; ?>
	</div>
<?php else : ?>
	<?php // output nothing ?>
<?php endif; ?>
<?php wp_reset_query(); ?>