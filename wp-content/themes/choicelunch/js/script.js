/* Author: Joey Yax @ Grady Britton

*/

// steps animation prep
//   Loop through steps to get positions from stylesheet

var API = {
	ApiKey: "JjmpMnPmdGrAV2Y2GUl4qucbentmvDLGQDhLSXX27UoZrS+/9YT6hnPRFjacCIjZeqTe42DN42EQfQ1PIbUX9AwuJY3EV3D004A6wKf6XRxra9R5l2B7JFSOd5cDi0Tv+svJXFukwDWwM2ECat2iLhGqIP5AbVCRTBbqyS4SdCD4p8YfxdiFKp0toaB4orbjaztsK/Ha4oi+TFCCTCJGpRYZtDqVc6aB9IC1X+pQmmv38m69Z3tsHTDqi37fd7xps95/yBJ4TNEgIyxowPVSppAV69aSOSOLwU4SCGFJ381n/a00LYGGjd5QCgo4OutkukgEhYaXlPBP9yURUxw+S332GMh1pAh1Jyvbh0Szj099KvjGMYCtWwlq9zirW1VAp6E627wPKnUJ8bAlnv4UBDXrTRDDgClacSzfaKr0C+PQHkRdDAHe9/QPmaKC4svBElD94fgAwu0aKZp6I+LXOH6dN0WERbyWWQ0BrPNmy4FmH43McJArreernasHI1oD",
	BaseServiceUrl: "//order.choicelunch.com/api/",
	AppPath: '//order.choicelunch.com',
	RedirectDomain: '//order.choicelunch.com/'
};

var steps_count = $('.step').length; // count steps
var step_tops = [];
for(var i = 0; i < steps_count; i++) {
	step_tops[i] = $('.step').eq(i).css('top');
}


var isMobile = {
    Android: function() { return navigator.userAgent.match(/Android/i) ? true : false; },
    BlackBerry: function() { return navigator.userAgent.match(/BlackBerry/i) ? true : false; },
    iOS: function() { return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false; },
    iPhone: function() { return navigator.userAgent.match(/iPhone|iPod/i) ? true : false; },
    iPad: function() { return navigator.userAgent.match(/iPad/i) ? true : false; },
    Windows: function() { return navigator.userAgent.match(/IEMobile/i) ? true : false; },
    any: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows()); }
};

function scroll_div(depl) {
	document.getElementById('scroll_container').scrollLeft -= depl;
	window.scroll_timer = setTimeout('scroll_div('+depl+')', 30);
}

function check4slideshow() {
	if ( !isMobile.iPhone() && !isMobile.Android() && !isMobile.BlackBerry() && !isMobile.Windows() ) {
		if ($('.slider').length > 0 && $('.slider').not('.instantiated')) {
			$('.slider').addClass('instantiated');

			var slider = $('.slider');
			var slides = $('.slide', slider);

			// overflow wrapper
			slider.wrapInner('<div class="slide_container"><div class="overflow"></div></div>');

			// adjust overflow
			var slide_count = slides.length;
			var slide_width = slides.width();
			$('.overflow', slider).width(slide_width * slide_count); // adjust size of container to fit slides
		
			// add nav arrows
			slider.append('<a class="nav prev" href="#"></a>');
			slider.append('<a class="nav next" href="#"></a>');

			// add pager
			slider.append('<div class="pager"></div>');
			for( var i=0; slide_count > i; i++ ) {
				$('.pager',slider).append('<a href="#" rel="' + i + '">&#9679;</a>');
			}

			// add active class to first slide
			$('.slide:first-child', slider).addClass('active');
			check_nav();
		}
	}
}

function prev_slide() {
	if (!$('.slide_container').hasClass('scrolling')) {
		$('.slide_container').addClass('scrolling');
		var curr_slide = $('.slide.active');
		curr_slide.removeClass('active');
		curr_slide.prev('.slide').addClass('active');
		$('.slide_container').stop().scrollTo($('.slide.active'), 800, function() {
			$('.slide_container').removeClass('scrolling');
		});
		check_nav();
	}
}

function next_slide() {
	if (!$('.slide_container').hasClass('scrolling')) {
		$('.slide_container').addClass('scrolling');
		var curr_slide = $('.slide.active');
		curr_slide.removeClass('active');
		curr_slide.next('.slide').addClass('active');
		$('.slide_container').stop().scrollTo($('.slide.active'), 800, function() {
			$('.slide_container').removeClass('scrolling');
		});
		check_nav();
	}
}

function goto_slide(slide_num) {
	if (!$('.slide_container').hasClass('scrolling')) {
		$('.slide_container').addClass('scrolling');
		$('.slide.active').removeClass('active');
		$('.slider .slide').eq(slide_num).addClass('active');
		$('.slide_container').stop().scrollTo($('.slide.active'), 800, function() {
			$('.slide_container').removeClass('scrolling');
		});
		check_nav();
	}
}

function check_nav() {
	
	// get active slide
	var active_slide = $('.slide_container .active').index();
	// hide next button if there is no next
	if (active_slide === 0) { $('.nav.prev').fadeOut(); }
	else { $('.nav.prev').fadeIn(); }

	// hide prev button if there is no prev
	if (active_slide === $('.slider .slide').length-1) { $('.nav.next').fadeOut(); }
	else { $('.nav.next').fadeIn(); }

	// update pager
	$('.slider .pager a.active').removeClass('active');
	$('.slider .pager a').eq(active_slide).addClass('active');


}

function hideShowVideo(doThis) {
	// set height to auto for mobile and old versions of ie
	if(! isMobile.any() && !$('html').hasClass('ie')) {
		switch(doThis) {
			case 'show': $('.intro-video').stop().animate({ height: $('.intro-video iframe').height() }, 500); break;
			case 'hide': if (! $('.intro-video').hasClass('do-not-collapse')) { $('.intro-video').stop().animate({ height:400 }, 500); } break;
		}
	}
	else {
		$('.intro-video').css('height', '');
	}
}

function onPause(id) {
	//status.text('paused');
	// console.log('video paused');
	$('.intro-video').removeClass('do-not-collapse');
}

function onFinish(id) {
	// collapse video player
	// console.log('video finished');
	$('.intro-video').removeClass('do-not-collapse');
	hideShowVideo('hide');
	_gaq.push(['_trackEvent', 'Video', 'Video - Finished', 'Choicelunch']);
	//status.text('finished');
}

function onPlay(id) {
	// console.log('video playing');
	$('.intro-video').addClass('do-not-collapse');
	_gaq.push(['_trackEvent', 'Video', 'Video - Play', 'Choicelunch']);
	//status.text('finished');
}

function onPlayProgress(data, id) {
	//status.text(data.seconds + 's played');
}

function _run( videoID ) {
	// All of the magic handled by SWFObject (http://code.google.com/p/swfobject/)
	swfobject.embedSWF("http://www.youtube.com/v/" + videoID + "&enablejsapi=1&playerapiid=videoOutput&version=3&controls=0&rel=0&showinfo=0", "iphone_screen", "235", "355", "8", null, null, { allowScriptAccess: "always" }, { id: "video" });
}

function loadVideo(videoID) {
	if (typeof(ytplayer) != "undefined") {
		ytplayer.loadVideoById(videoID);
	}
	else {
		_run( videoID );
	}
}

function onYouTubePlayerReady(playerId) {
	ytplayer = document.getElementById("video");
	ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
	playVideo();
}

function onytplayerStateChange(newState) {
	ytplayer.setPlaybackQuality('highres');
	//console.log('hi');
}

function playVideo() {
	if (ytplayer) { ytplayer.playVideo(); }
}

function pauseVideo() {
	if (ytplayer) { ytplayer.pauseVideo(); }
}

function collapse_food() {
	if($('.food_cat').length > 0 && $('body').width() < 767) {
		$('.food_cat').addClass('collapsed');
		$('.food_cat .category:not(.open)').slideUp();
	}
	else {
		$('.food_cat').removeClass('collapsed');
		$('.food_cat .category').slideDown();
	}
}

function collapse_food_detail() {
	if($('.food_details').length > 0 && $('body').width() < 767) {
		$('.food_details').addClass('collapsed');
		$('.food_details .content:not(.open)').slideUp();
	}
	else {
		$('.food_details').removeClass('collapsed');
		$('.food_details .content').slideDown();
	}
}

function responsive_scroller_nav() {
	if( isMobile.any() ) {
		$('.scrollable .nav').hide();
		
		if ( $('.scroll_directions').length === 0 ) {
			$('.scroll_container').css('overflow','auto').prepend('<div class="scroll_directions"><div class="hand"></div></div>');

			setInterval( function() {
				$('.scroll_directions .hand').animate({ left: '-50px', opacity: 0 }, 1000, function() {
					setTimeout( function() {
						$('.scroll_directions .hand').css('left','50px').animate({ opacity: 1 }, 500);
					}, 1000);
				});
			}, 3000);
		}
	}
	else {
		$('.scrollable .nav').show();
		$('.scroll_container').css('overflow','none');
	}
}

function ga_tracking() {

	$( '.nav li, .secondary-nav li' ).each( function() {
		var nav_li = $( this ),
			nav_a = nav_li.children( 'a' ),
			nav_placement = ( nav_li.parents( 'header' ).length > 0 ? 'Header ' : ( nav_li.parents( 'footer' ).length > 0 ? 'Footer ' : '' ) ),
			nav_type = ( nav_li.hasClass( "btn" ) ? "Button" : "Link" ),
			nav_text = nav_a.text();

		nav_a.attr( "onclick", "_gaq.push(['_trackEvent', '" + nav_placement + "Nav', 'Nav " + nav_type + " Click', '" + nav_text + "']);" );
	});
	
	// add gaq to comment submit button
	var form_btn_submit = $( '#comments-form #submit' );
	if ( form_btn_submit.length  > 0 ) {
		form_btn_submit.attr( "onclick", "_gaq.push(['_trackEvent', 'Blog', 'Submit Comment Button Click', '" + $( "h2" ).eq(0).text() + "'])" );
	}

}
ga_tracking();


$(document).ready(function(){

	$('.more_link').live('click',function(e){
		e.preventDefault();
		var href = $(this).attr('href');
		var section = ( $(this).data('section-title') != 'undefined' ? $(this).data('section-title') : 'Undefined Section' );
		console.log( section );
		if($(href).hasClass('hidden')){
			$(href).removeClass('hidden');
			$(this).html(' &laquo; Less');
			_gaq.push(['_trackEvent', 'Inline Link', 'More click', section]);
		}
		else{
			$(href).addClass('hidden');
			$(this).html('More &raquo;');
			_gaq.push(['_trackEvent', 'Inline Link', 'Less click', section]);
		}
	});

	// $('.modal').fancybox();
	
	$('.tothetop').live( 'click', function(e) {
		e.preventDefault();
		$('html, body').animate({scrollTop: 0}, 2000);
	});

	collapse_food();
	$(window).resize(function() {
		collapse_food();
	});

	$('.food_cat h3').click( function(e) {
		e.preventDefault();
		if($('body').width() < 767) {
			var cat_class = $(this).attr('class').replace('title_','');
				cat_class = '.category.'+cat_class;
			
			if ($('i',this).hasClass('icon-caret-right')) {
				$(cat_class).addClass('open').slideDown();
				$('i',this).removeClass('icon-caret-right').addClass('icon-caret-down');
			}
			else {
				$(cat_class).removeClass('open').slideUp();
				$('i',this).removeClass('icon-caret-down').addClass('icon-caret-right');
			}
		}
	});

	collapse_food_detail();
	$(window).resize(function() {
		collapse_food_detail();
	});

	$('.food_details h2').click( function(e) {
		e.preventDefault();

		if($('body').width() < 767) {
			var cat_class = $(this).attr('class').replace('title_','');
				cat_class = '.content.'+cat_class;

			if ($('i',this).hasClass('icon-caret-right')) {

				$('.food_details .content').removeClass('open').slideUp();
				$('.food_details h2 i').removeClass('icon-caret-down').addClass('icon-caret-right');

				$(cat_class).addClass('open').slideDown( function() {
					console.log( $('.content.open').parent().children('h2').offset().top );
					$('html, body').animate({scrollTop: $('.content.open').parent().children('h2').offset().top}, 1000);
				});
				$('i',this).removeClass('icon-caret-right').addClass('icon-caret-down');
			}
			else {
				$(cat_class).removeClass('open').slideUp();
				$('i',this).removeClass('icon-caret-down').addClass('icon-caret-right');
			}

		}
	});

	$('a[rel=attachment]').fancybox();
	if( ! isMobile.iPhone() ) {
		$('.popup').live('click', function(e) {
			e.preventDefault();
			var $this = $(this),
				href  = $this.attr( 'href' ),
				type  = null; // default type to null so modals displaying inline content will work

			//  check if the href starts with a hash - if not, set fancybox type to ajax
			if ( ! href.match( /^#([a-z0-9]*)/ ) ) { type = 'ajax'; }
			if ( href.match( /.+\.(jpe?g|gif|png)$/i ) ) { type = 'image'; }

			// call fancybox
			$.fancybox({
				href: href,
				type: type
			});
		});
	}
	
	$('.popup-investigator').live('click', function(e) {
		e.preventDefault();
		$this = $(this);
		$.fancybox({
			href: $this.attr('href') + '?content_only=true',
			type: 'ajax'
		});
	});

	$('.carousel').carousel({
		interval: 2000
	});

	$('.intro h1').fitText(1.2, { minFontSize: '30px', maxFontSize: '48px' });

	$('#navbar').scrollspy();

	$('.show-tooltip').tooltip();

	$('#ourfood_tabs .tabs li a').live('click', function(e) {
		e.preventDefault();
		$(this).parent().parent().children('.current_page_item').removeClass('current_page_item');
		$(this).parent('li').addClass('current_page_item');
		$.ajax({
			url: $(this).attr('href') + '?hidefood=true',
			async: false,
			cache: false,
			beforeSend: function() {
				if ($('#ourfood_tab_content').length < 1) {
					$('.topshadow').removeClass('topshadow');
					$('#ourfood_tabs').after('<section id="ourfood_tab_content" class="topshadow shadow_bottom hidden-tablet hidden-phone"></section>');
				}
				$('#ourfood_tab_content').html('<div class="loading">Loading...</div>');
			},
			success: function(data) {
				$('#ourfood_tab_content').html($(data).find('#ourfood_tab_content > *'));
				check4slideshow();
			}
		});
	});

	if ($('.expandable').length > 0) {
		$('.expandable span').hide();
		$('.expandable .expander').live('click', function(e) {
			e.preventDefault();
			var id_to_expand = $(this).attr('href');
			$(id_to_expand).fadeToggle();
		});
	}


	check4slideshow();
	
	
	$('.slider .nav.prev').live('click', function(e) {
		e.preventDefault();
		if ( $(".tabs .current_page_item a").length  > 0 )
		{
			_gaq.push(['_trackEvent', 'Slider Arrows', 'Left Arrow Click', $(".tabs .current_page_item a").text()]);
		}
		prev_slide();
	});

	$('.slider .nav.next').live('click', function(e) {
		e.preventDefault();
		if ( $(".tabs .current_page_item a").length  > 0 )
		{
			_gaq.push(['_trackEvent', 'Slider Arrows', 'Right Arrow Click', $(".tabs .current_page_item a").text()]);
		}
		next_slide();
	});

	$('.slider .pager a').live('click', function(e) {
		e.preventDefault();
		goto_slide($(this).index());
	});


	// scrollable
	

	if ($('.scrollable').length > 0) {
		$('.scrollable').addClass('instantiated');

		var scroller = $('.scrollable');

		// overflow wrapper
		scroller.wrapInner('<div id="scroll_container" class="scroll_container"><div class="overflow"></div></div>');
		
		// add nav arrows
		scroller.append('<a class="nav back" href="#"></a>');
		scroller.append('<a class="nav forward" href="#"></a>');
		
		
		$(window).resize(function() {
			responsive_scroller_nav();
		});
		responsive_scroller_nav();

		// adjust overflow
		var img_width = parseFloat($('img', scroller).attr('width').replace('px',''));
		var nav_width = $('.nav', scroller).outerWidth();
		var new_width = img_width+(nav_width*2);
		
		$('.overflow', scroller).width(new_width);
		$('.scroll_container').css( 'padding-left', nav_width+20 );

		// add active class to first slide
		// check_scroll_btns();
	}

	$('.scrollable .nav.forward').live('mousedown', function(e) {
		scroll_div(-15);
		e.preventDefault();
	});

	$('.scrollable .nav.forward').live('mouseup', function(e) {
		e.preventDefault();
		clearTimeout(window.scroll_timer);
	});

	$('.scrollable .nav.forward').live('click', function(e) {
		e.preventDefault();
		if ( $(".navbar .current_page_item a").length  > 0 )
		{
			_gaq.push(['_trackEvent', 'Slider Arrows', 'Right Arrow Click', $(".navbar .current_page_item a").text()]);
		}
		clearTimeout(window.scroll_timer);
	});

	$('.scrollable .nav.back').live('mousedown', function(e) {
		scroll_div(15);
		e.preventDefault();
	});

	$('.scrollable .nav.back').live('mouseup', function(e) {
		e.preventDefault();
		clearTimeout(window.scroll_timer);
	});
	$('.scrollable .nav.back').live('click', function(e) {
		e.preventDefault();
		if ( $(".navbar .current_page_item a").length  > 0 )
		{
			_gaq.push(['_trackEvent', 'Slider Arrows', 'Left Arrow Click', $(".navbar .current_page_item a").text()]);
		}
		clearTimeout(window.scroll_timer);
	});


	// homepage vimeo

	if ($('.intro-video').length > 0) {

		hideShowVideo('hide');

		$(window).resize(function() {
			hideShowVideo('hide');
		});

		$('.intro-video iframe').hover(function() {
			hideShowVideo('show');
		},
		function() {
			hideShowVideo('hide');
		});

		$('.intro-video').fitVids(); // size video
		
		var iframe = $('#v1')[0],
			player = $f(iframe),
			status = $('.status');

		// When the player is ready, add listeners for pause, finish, and playProgress
		player.addEvent('ready', function() {
			//status.text('ready');

			player.addEvent('pause', onPause);
			player.addEvent('finish', onFinish);
			player.addEvent('play', onPlay);
			player.addEvent('playProgress', onPlayProgress);
		});

		// Call the API when a button is pressed
		$('button').bind('click', function() {
			player.api($(this).text().toLowerCase());
		});

	}

	function onYouTubePlayerReady(playerId) {
			//console.log('ytplayer');
		}
		
jQuery('.videos a').click( function(e) {
	// if on ios follow link to youtube since we can't play video inline.
	if (!isMobile.any()) {
		e.preventDefault();
		var video = jQuery(this).attr('vid');
		
		if ( jQuery(this).hasClass('playing') ) {
			pauseVideo();
			jQuery( '.videos a' ).removeClass('paused');
			jQuery( '.videos a' ).removeClass('playing');
			jQuery( this ).addClass('paused');
		}
		else if ( jQuery(this).hasClass('paused') ) {
			playVideo();
			jQuery( '.videos a' ).removeClass('paused');
			jQuery( '.videos a' ).removeClass('playing');
			jQuery( this ).addClass('playing');
		}
		else {
			loadVideo( video );
			jQuery( '.videos a' ).removeClass('paused');
			jQuery( '.videos a' ).removeClass('playing');
			jQuery( this ).addClass('playing');
		}
	}
});

jQuery('.iphone .screen').append('<img class="inner_shadow" src="http://order.choicelunch.com/marketing/landing/images/screen_shadow_large.png" alt="" />').hide().fadeIn(200);
if(!isMobile.any()) {
	jQuery('.iphone .screen').append('<img class="btn_play" src="http://order.choicelunch.com/marketing/landing/images/btn_play_large.png" alt="">');

	jQuery('.iphone .screen').click( function() {
		jQuery('.videos a').eq(0).click();
	});

	jQuery('.iphone .screen').hover( function() {
		jQuery('.inner_shadow',this).fadeOut(150);
		jQuery('.btn_play').attr('src','http://order.choicelunch.com/marketing/landing/images/btn_play_hover_large.png').fadeIn(150);
		//jQuery('.btn_play',this).fadeIn(200);
	}, function() {
		jQuery('.inner_shadow',this).fadeIn(150);
		jQuery('.btn_play').attr('src','http://order.choicelunch.com/marketing/landing/images/btn_play_large.png').fadeIn(150);
		//jQuery('.btn_play',this).fadeOut(200);
	});
}
	
	
});

$(window).load(function() {

	function push_footer() {
		$('footer').css('margin-top', 0);

		var viewport_height = $(document).height();
		var html_height = $('body').outerHeight();
		// console.log('viewport: ' + viewport_height);
		// console.log('html: ' + html_height);

		if (html_height < viewport_height) {
			var footer_height = $('footer').outerHeight();
			// console.log('footer: ' + footer_height);
			var footer_top_margin = (viewport_height - html_height);
			// console.log('footer margin: ' + footer_top_margin);
			$('footer').css('margin-top', footer_top_margin);
		}
	}

	$(window).bind('hashchange', function() {
		var curr_pos = $( window ).scrollTop();
		$( window ).scrollTop( curr_pos - 140 );
	});

	if ( window.location.hash ) {
		var curr_pos = $( window ).scrollTop();
		$( window ).scrollTop( curr_pos - 140 );
	}

	// console.log('page loaded 100%');
	$('body').addClass('loading-complete'); // add loading complete to the body

	$('.content-loading').remove();
	$('.waitforload').show();

	// push footer to bottom of page
	push_footer();
	$(window).resize(push_footer);

});

(function ($) {
	// Start Serialize Object
	$.fn.serializeObject = function (options) {
		options = jQuery.extend({}, options);
		var self = this, json = {}, push_counters = {},
			patterns = {
				"validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
				"key": /[a-zA-Z0-9_]+|(?=\[\])/g,
				"push": /^$/,
				"fixed": /^\d+$/,
				"named": /^[a-zA-Z0-9_]+$/
			};

		this.build = function (base, key, value) {
			base[key] = value;
			return base;
		};

		this.push_counter = function (key) {
			if (push_counters[key] === undefined) {
				push_counters[key] = 0;
			}
			return push_counters[key]++;
		};

		jQuery.each(jQuery(this).serializeArray(), function () {
			// skip invalid keys
			if (!patterns.validate.test(this.name)) {
				return;
			}
			var k, keys = this.name.match(patterns.key), merge = this.value,
				reverse_key = this.name;

			while ((k = keys.pop()) !== undefined) {
				// adjust reverse_key
				reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
				// push
				if (k.match(patterns.push)) {
					merge = self.build([], self.push_counter(reverse_key), merge);
				}
				// fixed
				else if (k.match(patterns.fixed)) {
					merge = self.build([], k, merge);
				}
				// named
				else if (k.match(patterns.named)) {
					merge = self.build({}, k, merge);
				}
			}
			json = jQuery.extend(true, json, merge);
		});
		return json;
	};
	//End Serialize Object
})(jQuery);

var HEADERS = {
		'content-type': 'application/json'
	},
	CL = {};

CL.Util = {
	addAjaxLoader: function(scope){
		var loader = '<span class="ajax-loader"></span>';
		if(scope.find('.ajax-loader')){
			scope.find('.ajax-loader').remove();
		}
		scope.addClass('loaderIn');
		scope.append(loader);
		scope.attr('disabled', 'disabled');
	},
	removeAjaxLoader: function(scope){
		scope.removeClass('loaderIn');
		scope.find('.ajax-loader').remove();
		scope.removeAttr('disabled');
	},

	doAPIRequest: function (config) {
		var self = this;
		this.addAjaxLoader(config.custom_loader);
		var headers = $.extend({}, HEADERS, (config.extra_headers || {}));
		$.ajax({
			url: config.url,
			type: config.method || 'GET',
			crossDomain: true,
			headers: headers,
			data: config.params || {},
			success: function (response) {
				config.success && config.success(response);
			},

			error: function (response) {
				config.error && config.error(response);
			},

			complete: function () {
				self.removeAjaxLoader(config.custom_loader);
			}
		});
	}
}

$('#signin-form').find("button[type=submit]").on("click", function(e){
	e.preventDefault();
	var jForm = $("#signin-form"),
		el = $(this), params;

	if(jForm.valid()) {
		params = jForm.serializeObject();
		jForm.find("#general-error").hide().html("");
		CL.Util.doAPIRequest({
			url: API.BaseServiceUrl + 'account/token',
			custom_loader: el,
			extra_headers: {
				cl_api_key: API.ApiKey,
				cl_username: params.username,
				cl_password: params.password
			},
			success: function (response) {
				if(response.errorMessage){
					jForm.find("#general-error").show().html(response.errorMessage);
				}else {
					API.AccountToken = response.token;
					if(response.hasSharedCredentials){
						window.location = API.RedirectDomain + 'ChooseAccount.aspx?lid=' + response.loginID;
					}else {
						$.ajax({
							url: API.AppPath + '/GetCookie.ashx',
							type: 'POST',
							crossDomain: true,
							xhrFields: {
								withCredentials: true
							},
							data: JSON.stringify({
								cl_token: API.AccountToken,
								isPersistent: false
							}),
							complete: function () {
								$(".btn-orange.dropdown a").dropdown('toggle');
								window.location = API.RedirectDomain;
							}
						});
					}
				}
			},
			error: function (response) {
				try {
					var res = JSON.parse(response.responseText);
					jForm.find("#general-error").show().html(res.errorMessage || "Something went wrong! Please try again");
				}catch(e) {
					jForm.find("#general-error").show().html("Something went wrong! Please try again");
				}
			}
		});
	}
})
