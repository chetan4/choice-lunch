<?php

register_nav_menu( 'primary', __( 'Primary Menu', 'choicelunch' ) );
register_nav_menu( 'secondary', __( 'Secondary Menu', 'choicelunch' ) );
register_nav_menu( 'footer1', __( 'Footer Menu, Column 1', 'choicelunch' ) );
register_nav_menu( 'footer2', __( 'Footer Menu, Column 2', 'choicelunch' ) );
register_nav_menu( 'our_food', __( 'Our Food Menu', 'choicelunch' ) );
add_theme_support( 'post-thumbnails' ); 

// read more for excerpts
function new_excerpt_more($more) {
       global $post;
	return '&hellip;';
}

add_filter('excerpt_more', 'new_excerpt_more');


// Comment Layout
function choicelunch_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<div class="comment-author vcard row-fluid clearfix">
				<div class="avatar span2">
					<?php echo get_avatar($comment,$size='75',$default='<path_to_url>' ); ?>
				</div>
				<div class="span10 comment-text">
					<?php printf(__('<h4 style="margin-top: 0;">%s <small><small>on ' . get_comment_time('M jS') . ' @ ' . get_comment_time('g:i A') . ' said:</small></small></h4>','choicelunch'), get_comment_author_link()) ?>
					<?php if ($comment->comment_approved == '0') : ?>
       					<div class="alert-message success">
          				<p><?php _e('Your comment is awaiting moderation.','choicelunch') ?></p>
          				</div>
					<?php endif; ?>
                    
                    <?php comment_text() ?>
                    
                    <?php edit_comment_link(__('Edit Comment','choicelunch'),'',' | ') ?>
                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                	
                    
                    
                </div>
			</div>
		</article>
    <!-- </li> is added by wordpress automatically -->
<?php
} // don't remove this bracket!

// Display trackbacks/pings callback function
function list_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
?>
        <li id="comment-<?php comment_ID(); ?>"><i class="icon icon-share-alt"></i>&nbsp;<?php comment_author_link(); ?>
<?php 

}

// Only display comments in comment count (which isn't currently displayed in wp-bootstrap, but i'm putting this in now so i don't forget to later)
add_filter('get_comments_number', 'comment_count', 0);
function comment_count( $count ) {
	if ( ! is_admin() ) {
		global $id;
	    $comments_by_type = &separate_comments(get_comments('status=approve&post_id=' . $id));
	    return count($comments_by_type['comment']);
	} else {
	    return $count;
	}
}


add_editor_style('editor-style.css');

// Add Twitter Bootstrap's standard 'active' class name to the active nav link item

add_filter('nav_menu_css_class', 'add_active_class', 10, 2 );
function add_active_class($classes, $item) {
	if($item->menu_item_parent == 0 && in_array('current-menu-item', $classes)) {
        $classes[] = "active";
	}
    return $classes;
}


function register_custom_post_types() {

  // register_taxonomy( 'products_category', 'products', array( 'hierarchical' => true, 'label' => 'Categories', 'query_var' => 'products_category', 'rewrite' => array(  'slug' => 'products/category', 'with_front' => false ) ) );  

  $cpt_slug = 'investigator';
  $cpt_name_singular = 'investigator';
  $cpt_name_plural = 'investigators';
  $cpt_title_singular = 'investigator';
  $cpt_title_plural = 'investigators';
  register_post_type( $cpt_slug, array(
    'public'            => true,
    'rewrite'           => $cpt_slug,
    'can_export'        => true,
    'hierarchal'        => false,
    'has_archive'       => true,
    'capability_type'   => $cpt_slug,
    'capabilities'      => array(
      'edit_post'         => 'edit_' . $cpt_name_singular,
      'read_post'         => 'read_' . $cpt_name_singular,
      'delete_post'       => 'delete_' . $cpt_name_singular,
      'edit_posts'        => 'edit_' . $cpt_name_plural,
      'edit_others_posts' => 'edit_others_' . $cpt_name_plural,
      'publish_posts'     => 'publish_' . $cpt_name_plural,
      'read_private_posts'=> 'read_private_' . $cpt_name_plural
    ),
    'map_meta_cap'      => true,
    'supports'          => array( 'title', 'editor', 'thumbnail' ),
    'show_in_nav_menus' => false,
    'labels'            => array(
      'name'               => ucfirst( $cpt_title_plural ),
      'singular_name'      => ucfirst( $cpt_title_singular ),
      'add_new'            => 'Add New',
      'all_items'          => 'All ' . ucfirst( $cpt_title_plural ),
      'add_new_item'       => 'Add New ' . ucfirst( $cpt_title_singular ),
      'edit_item'          => 'Edit ' . ucfirst( $cpt_title_plural ),
      'new_item'           => 'New ' . ucfirst( $cpt_title_singular ),
      'view_item'          => 'View ' . ucfirst( $cpt_title_plural ),
      'search_items'       => 'Search ' . ucfirst( $cpt_title_plural ),
      'not_found'          => 'No ' . $cpt_title_plural . ' listings found',
      'not_found_in_trash' => 'No ' . $cpt_title_plural . ' found in Trash'
      )
    )
  );

  $cpt_slug = 'chef';
  $cpt_name_singular = 'chef';
  $cpt_name_plural = 'chefs';
  $cpt_title_singular = 'chef';
  $cpt_title_plural = 'chefs';
  register_post_type( $cpt_slug, array(
    'public'            => true,
    'rewrite'           => $cpt_slug,
    'can_export'        => true,
    'hierarchal'        => false,
    'has_archive'       => true,
    'capability_type'   => $cpt_slug,
    'capabilities'      => array(
      'edit_post'         => 'edit_' . $cpt_name_singular,
      'read_post'         => 'read_' . $cpt_name_singular,
      'delete_post'       => 'delete_' . $cpt_name_singular,
      'edit_posts'        => 'edit_' . $cpt_name_plural,
      'edit_others_posts' => 'edit_others_' . $cpt_name_plural,
      'publish_posts'     => 'publish_' . $cpt_name_plural,
      'read_private_posts'=> 'read_private_' . $cpt_name_plural
    ),
    'map_meta_cap'      => true,
    'supports'          => array( 'title', 'editor' ),
    'show_in_nav_menus' => false,
    'labels'            => array(
      'name'               => ucfirst( $cpt_title_plural ),
      'singular_name'      => ucfirst( $cpt_title_singular ),
      'add_new'            => 'Add New',
      'all_items'          => 'All ' . ucfirst( $cpt_title_plural ),
      'add_new_item'       => 'Add New ' . ucfirst( $cpt_title_singular ),
      'edit_item'          => 'Edit ' . ucfirst( $cpt_title_plural ),
      'new_item'           => 'New ' . ucfirst( $cpt_title_singular ),
      'view_item'          => 'View ' . ucfirst( $cpt_title_plural ),
      'search_items'       => 'Search ' . ucfirst( $cpt_title_plural ),
      'not_found'          => 'No ' . $cpt_title_plural . ' listings found',
      'not_found_in_trash' => 'No ' . $cpt_title_plural . ' found in Trash'
      )
    )
  );

  $cpt_slug = 'testimonial';
  $cpt_name_singular = 'testimonial';
  $cpt_name_plural = 'testimonials';
  $cpt_title_singular = 'testimonial';
  $cpt_title_plural = 'testimonials';
  register_post_type( $cpt_slug, array(
    'public'            => true,
    'rewrite'           => $cpt_slug,
    'can_export'        => true,
    'hierarchal'        => false,
    'has_archive'       => true,
    'capability_type'   => $cpt_slug,
    'capabilities'      => array(
      'edit_post'         => 'edit_' . $cpt_name_singular,
      'read_post'         => 'read_' . $cpt_name_singular,
      'delete_post'       => 'delete_' . $cpt_name_singular,
      'edit_posts'        => 'edit_' . $cpt_name_plural,
      'edit_others_posts' => 'edit_others_' . $cpt_name_plural,
      'publish_posts'     => 'publish_' . $cpt_name_plural,
      'read_private_posts'=> 'read_private_' . $cpt_name_plural
    ),
    'map_meta_cap'      => true,
    'supports'          => array( 'title', 'editor' ),
    'show_in_nav_menus' => false,
    'labels'            => array(
      'name'               => ucfirst( $cpt_title_plural ),
      'singular_name'      => ucfirst( $cpt_title_singular ),
      'add_new'            => 'Add New',
      'all_items'          => 'All ' . ucfirst( $cpt_title_plural ),
      'add_new_item'       => 'Add New ' . ucfirst( $cpt_title_singular ),
      'edit_item'          => 'Edit ' . ucfirst( $cpt_title_plural ),
      'new_item'           => 'New ' . ucfirst( $cpt_title_singular ),
      'view_item'          => 'View ' . ucfirst( $cpt_title_plural ),
      'search_items'       => 'Search ' . ucfirst( $cpt_title_plural ),
      'not_found'          => 'No ' . $cpt_title_plural . ' listings found',
      'not_found_in_trash' => 'No ' . $cpt_title_plural . ' found in Trash'
      )
    )
  );

}


function register_custom_meta_boxes() {
  global $post;
  //echo 'post id: ' . $post->ID;
  $post = get_post($_GET['post']);
  add_meta_box( 'chef_photo', 'Chef Photo', 'chef_photo', 'chef', 'normal', 'high' );

  // add_meta_box( 'ingredient_investigators', 'Slide Elements', 'ingredient_investigators', 'ingredient_investigators', 'normal', 'high' );
  
}

// Hooks
add_action( 'init', 'register_custom_post_types' );
add_action( 'save_post',  'chef_photo_save' );
add_action( 'admin_menu', 'register_custom_meta_boxes' );


function chef_photo() {
  global $post;
  $custom = get_post_custom($post->ID);
  $chef_photo = $custom["chef_photo"][0];
  
  include('admin/meta-chef-photo.php');
}

function chef_photo_save() {
  global $post;
  
  /* Do nothing on auto save. */
  if ( defined( 'DOING_AUTOSAVE' ) && true === DOING_AUTOSAVE ) {
    return;
  }
  
  /* Save post meta. */
  update_post_meta($post->ID, "chef_photo", $_POST["chef_photo"]);
}


function load_template_part($template_name, $part_name=null) {
    ob_start();
    get_template_part($template_name, $part_name);
    $var = ob_get_contents();
    ob_end_clean();
    return $var;
}

// shortcodes

// [investigators]
function investigators_func( $atts ) {
	ob_start();
    get_template_part( 'part', 'investigators' );
    $output = ob_get_contents();
    ob_end_clean();

	return $output;
}
add_shortcode( 'investigators', 'investigators_func' );


// [chefs]
function chefs_func( $atts ) {
	ob_start();
    get_template_part( 'part', 'chefs' );
    $output = ob_get_contents();
    ob_end_clean();

	return $output;
}
add_shortcode( 'chefs', 'chefs_func' );

// [testimonials]
function testimonials_func( $atts ) {
	ob_start();
    get_template_part( 'part', 'testimonials' );
    $output = ob_get_contents();
    ob_end_clean();

	return $output;
}
add_shortcode( 'testimonials', 'testimonials_func' );


function convert2class( $str ) {
  $output = strtolower(preg_replace('/[^a-zA-Z0-9]/', '', $str));
  return $output;
}



