<?php
/*
Template Name: Food
 */
?>
<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span8 offset2 intro">
              <?php
                $pid = ($post->post_parent?$post->post_parent:$post->ID);
                $page = get_page($pid);
              ?>
              <h1><?php echo ( function_exists('the_subheading') && get_the_subheading($pid) != '' ? get_the_subheading($pid) : get_the_title($pid) ); ?></h1>
              <?php echo $page->post_content; ?>
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>
      </section>

      <!-- <div class="content-loading">Loading...</div> -->

      <section id="mobile_details" class="hidden-tablet hidden-desktop">
        <div class="container">
          <div class="row">
            <div class="span12" style="text-align: center; margin-bottom: 30px;">
              <p class="well"><span style="font-size: 17px;">Learn more about what goes into making our food great</span><br />
                <a class="btn btn-large btn-orange btn-block" style="margin-top: 20px;" href="<?php echo get_page_link(1694); ?>">Check it out!</a>
              </p>
            </div>
          </div>
        </div>
      </section>

      <?php
        if ( has_nav_menu('our_food') ) : ?>
        <section id="ourfood_tabs" class="hidden-tablet hidden-phone">
          <div class="container">
            <div class="row">
              <div class="span12">
                  <?php wp_nav_menu( array( 'theme_location' => 'our_food', 'depth' => 1, 'container' => false, 'container_class' => '', 'menu_class' => '', 'items_wrap' => '<ul id="%1$s" class="tabs">%3$s</ul>' ) ); ?>
              </div>
            </div>
          </div>
        </section>
        
        <?php if ( $post->post_parent ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <section id="ourfood_tab_content" class="topshadow shadow_bottom hidden-tablet hidden-phone">
          <div class="container">
            <div class="row">
              <div class="span12">
                <?php the_content(); ?>
              </div>
            </div>
          </div>
        </section>
        <?php endwhile; endif; endif; endif; ?>
        
        <?php if ( ! $post->post_parent ) : ?>
        <section id="food" class="topshadow">
        <?php else : ?>
        <section id="food">
        <?php endif; ?>
          <div class="container">
            <div class="row">
              <div class="span12 food_cat">

                <?php

                  $use_cache = false; // cache entrees
                  $cache_seconds = 3; // how many hours to pull from cache
                  $check_img = false; // check if image exists?

                  function cl_api() {
                    $url = "http://api.choicelunch.com/MenuItem/index";
                    $cl_api_key = 'JjmpMnPmdGrAV2Y2GUl4qucbentmvDLGQDhLSXX27UoZrS+/9YT6hnPRFjacCIjZeqTe42DN42EQfQ1PIbUX9AwuJY3EV3D004A6wKf6XRxra9R5l2B7JFSOd5cDi0Tv+svJXFukwDWwM2ECat2iLhGqIP5AbVCRTBbqyS4SdCD4p8YfxdiFKp0toaB4orbjaztsK/Ha4oi+TFCCTCJGpRYZtDqVc6aB9IC1X+pQmmv38m69Z3tsHTDqi37fd7xps95/yBJ4TNEgIyxowPVSppAV69aSOSOLwU4SCGFJ381n/a00LYGGjd5QCgo4OutkukgEhYaXlPBP9yURUxw+S332GMh1pAh1Jyvbh0Szj099KvjGMYCtWwlq9zirW1VAp6E627wPKnUJ8bAlnv4UBDXrTRDDgClacSzfaKr0C+PQHkRdDAHe9/QPmaKC4svBElD94fgAwu0aKZp6I+LXOH6dN0WERbyWWQ0BrPNmy4FmH43McJArreernasHI1oD';
                    $cl_token = 'WjgMre2lWqWiRMx3f59vUvpuEvCk/e/Xoo9Kx01/KxOsoAaKvL5cpAGCt3k9l7IzHD+rb3m+GMLOQqW1fe+WZVeXBTAwKk7UOb4FO4CaoWaVTdumrI11hD9znXQJpf0BdFgaExnsJYQd4q9iMJHk32ek1b+noC4VLRQ+0AgeBjtTpGJLxuXFchbDmwD1tq4TeYqvuEGQA4ByEMZOb62PTHZaMRYWO/QiW/8lgAcsbDWYzGawX4S0PU2bR1kHiOIYkG8Wgjzzj3KuF86PDjWfi2R58b6u1h7MFIZ3XjuU8s+V/oOm5iecd5P6Li9ULnHDdEn8eeoQCmyrK4nsA3eHhINK8CBn1F2f5nIvx7lZXL3TIDIcIjO9pw42GHttOkOf24HAPCQys7WG/nZdV60aFcVKeia+tPZR3BYg4kzj/AlozyCdi2n1Gipq6kjsTw9Df28DiO3Qza+e16nkKZoWb/J5XNXdl8idm04BKpLlIhzrJsd11H0R3xXsMu5erU41';

                    $ch = curl_init( $url );
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
                    curl_setopt( $ch, CURLOPT_HEADER, 0 );
                    curl_setopt( $ch, CURLOPT_ENCODING, 'gzip' );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
                      'cl_api_key: ' .$cl_api_key,
                      'cl_token: ' . $cl_token
                    ));

                    $data = curl_exec( $ch );
                    $info = curl_getinfo( $ch );
                    curl_close( $ch );

                    // echo $data;
                    return $data;
                    
                  }

                  if ( isset( $_GET['hidefood'] ) && $_GET['hidefood'] == 'true' ) {
                      // don't output food (to increase load time for ajax tabs)
                  }
                  else {

                    // cache
                    if ( $use_cache == true ) {
                      $datetime = date( 'Y-m-d H:i:s' );
                      $s = mysql_query( "SELECT * FROM food_cache WHERE food_cache_exp > '$datetime' LIMIT 1" ) or die( mysql_error() );
                      $c = mysql_num_rows( $s );
                      // if cache item found, pull data from cache
                      if ( $c > 0 ) {
                        while($r = mysql_fetch_assoc( $s )) {
                          $data = $r['food_data'];
                        }
                        $data = urldecode( $data );
                        $json = json_decode( $data );
                        // print_r($data);
                      }
                      // if no cache, populate cache
                      else {
                        $data = cl_api();
                        $json = json_decode( $data );
                        $data_encoded = urlencode( $data );
                        echo $data_encoded;
                        $datetime_exp = date( 'Y-m-d H:i:s', time() + ( $cache_seconds * 3600 ) );
                        $i = mysql_query( "INSERT INTO food_cache ( food_data, food_datetime, food_cache_exp ) VALUES( '$data_encoded', '$datetime', '$datetime_exp' )" )  or die( mysql_error() );
                      }
                    }
                    else {
                      $data = cl_api();
                      $json = json_decode( $data ); // decode json
                    }

                    function endsWith($haystack, $needle){ // filter strings ending with $needle and return true/false
                      return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
                    }
                    function categorize_food( $food, $type ) {
                      foreach( $food as $k => $v ) {    // exclude if Item Name ends with '+' and is not Item Type 'Lunch Break' 
                        if($v->ProductType == $type && endsWith($v->ExtendedItemName, "+") == false && $v->ItemType != "Lunch Break") {
                          $output[$v->ItemType][] = $v;
                        }
                      }
                      return $output;
                    }
                    $select_food = 'ENTREE';
                    $categorized_food = categorize_food( $json, $select_food );
                    ksort( $categorized_food ); // sort alphabetically

                    //echo '<h6>Product Type: '.$select_food.'</h6><pre style="height:300px;overflow:auto;"> ' . print_r( $categorized_food, 1 ) . '</pre>'; // output data

                    if ( count( $categorized_food ) > 0 ) {
                     
                      foreach( $categorized_food as $category => $entrees ) {
                        $entree_count = count( $entrees );

                        echo '<h3 class="title_' . convert2class($category) . '">' . $category . ' <i class="icon-caret-right"></i></h3>';
                        echo '<div class="category ' . convert2class($category) . '">';
                        for( $i = 0; $i < $entree_count; $i++ ) {
                          
                          // create row after every 4th entree
                          if ( $i % 4 == 0 ) { echo '<div class="row">'; }
                          
                          // allergens
                          foreach( $entrees[$i]->Allergens as $allergen ) {
                            $allergens[] = $allergen->AllergenName;
                          }

                          // thumbnail
                          
                          // ** check if image exists
                          if ( $check_img == true ) {
                            $ch = curl_init( $entrees[$i]->ItemURL );
                            curl_setopt($ch, CURLOPT_NOBODY, true);
                            curl_exec($ch);
                            $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                            curl_close($ch);

                            if ( $retcode == 200 ) { $thumbnail = get_bloginfo( 'template_directory' ) . '/mk_thumb.php?src=' . str_replace('mobile/','',$entrees[$i]->ItemURL) . '&h=236&w=338&q=100'; }
                            else { $thumbnail = get_bloginfo( 'template_directory' ) . '/img/missing-food-image.png'; }
                          }
                          else {
                            $thumbnail = get_bloginfo( 'template_directory' ) . '/mk_thumb.php?src=' . str_replace('mobile/','',$entrees[$i]->ItemURL) . '&h=236&w=338&q=100';
                          }

                          //
                          // determine entree 'features'
                          //
                          
                          $entree_etc = array();

                          // No meat: Display when “IsVegetarian=true”
                          if ( $entrees[$i]->IsVegetarian == 'true' ) {
                            $entree_etc[] = '<img class="show-tooltip" src="' . get_bloginfo( 'template_directory' ) . '/img/icon-no-meat.png" alt="Vegetarian" title="Vegetarian" />';
                          }

                          // No dairy: Display when Allergen list does NOT contain Milk
                          if ( is_array( $allergens ) && ! in_array( 'Milk', $allergens ) ) {
                            $entree_etc[] = '<img class="show-tooltip" src="' . get_bloginfo( 'template_directory' ) . '/img/icon-no-dairy.png" alt="No Dairy" title="No Dairy" />';
                          }

                          // No wheat: Display when Allergen list does NOT contain Wheat AND does not contain Gluten
                          if ( is_array( $allergens ) && ! in_array( 'Wheat', $allergens ) && ! in_array( 'Gluten', $allergens ) ) {
                            $entree_etc[] = '<img class="show-tooltip" src="' . get_bloginfo( 'template_directory' ) . '/img/icon-no-gluten.png" alt="No Gluten" title="No Gluten" />';
                          }

                          // output
                          echo '<a class="span3 entree">
                                <div class="inner">
                                  <div class="thumb"><img src="' . $thumbnail . '" alt="" title="" /></div>
                                  <div class="title">' . $entrees[$i]->ExtendedItemName . '</div>
                                  <div class="detail">' . $entrees[$i]->Description . '</div>
                                  <div class="etc">' . ( is_array( $entree_etc ) ? implode( ' ', $entree_etc ) : '' ) . '</div>
                                </div>
                              </a>';

                          // ^ allergens
                          // <div class="etc">' . ( is_array( $allergens ) ? 'Allergens:' . implode( ', ', $allergens ) : '' ) . '</div>

                          // close row after every 4th row or after last entree in cat
                          if ( $i % 4 == 3 || $i == $entree_count - 1 ) { echo '</div>'; }
                          unset( $allergens );
                          unset( $entree_etc );
                        }
                        unset( $entree_count );
                        // echo '<img src="' . $v->ItemURL . '" />';
                        // echo '<pre>' . print_r( $v, 1 ) . '</pre>'; // output data
                        echo '</div>';
                      }
                    }
                  }

                ?>
              </div>
            </div>
          </div>
        </section>

      <section id="related_posts" class="">
        <div class="container">
          <div class="row hidden-phone">
            <div class="span12">
              <h3>Related Blog Posts</h3>
            </div>
          </div>
          <div class="row">
            <?php query_posts( array( 'posts_per_page' => '3' ) ); ?>
            <?php
              function custom_excerpt_length( $length ) { return 22; }
              add_filter( 'excerpt_length', 'custom_excerpt_length' );

              if ( have_posts() ) :
                while ( have_posts() ) :
                  the_post();
                  $custom = get_post_custom( $post->ID );
            
                  $src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); 
                ?>
                <div class="span3 hidden-phone">
            <?php 
                 if($src) { // thumbnail
                  echo '<a class="thumb" href="';
                  the_permalink();
                  echo '"><img class="round-border" src="'. get_bloginfo( "template_directory" ) . '/mk_thumb.php?src=' . $src . '&h=120&w=270" alt="" /></a>';
                }; ?>
                  <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                  <div class="meta"><?php the_time( 'M j, Y' ); ?></div>
                  <div class="detail"><?php the_excerpt(); ?></div>
                </div>
                <?php endwhile; ?>
            <?php else : ?>
              <?php // output nothing ?>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <div class="span3 callout">
              <p class="hidden-phone"><a class="btn btn-large btn-orange btn-block" href="https://order.choicelunch.com" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Sign In / Order']);">Sign In / Order</a></p>
              <p class="hidden-desktop hidden-tablet"><a class="btn btn-large btn-orange btn-block" href="https://order.choicelunch.com" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Sign In / Order']);">Sign In / Order</a></p>
              <p class="hidden-phone"><a class="btn btn-green btn-block" href="http://order.choicelunch.com/demo" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Try a Live Demo']);">Try a Live Demo</a></p>
              <h3>Taste the Awesome<sup>&reg;</sup></h3>
              <p><a href="https://order.choicelunch.com/Account/Registration/RegistrationCode.aspx" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Link Click', 'Find out how...']);">Find out how you can bring Choicelunch to your school, pre-school or summer camp.</a></p>
            </div>
          </div>
        </div>
      </section>



    </div>
<?php get_footer(); ?>
<script type="text/javascript">
	$("#menu-item-1680").click(function () {_gaq.push(['_trackEvent', 'Our Food Tabs', 'Tab Link Click', 'Tab - Our Chefs']);});		
	$("#menu-item-1682").click(function () {_gaq.push(['_trackEvent', 'Our Food Tabs', 'Tab Link Click', 'Tab - Our Ingredients']);});
	$("#menu-item-1681").click(function () {_gaq.push(['_trackEvent', 'Our Food Tabs', 'Tab Link Click', 'Tab - Our Process']);});
	$("#menu-item-1679").click(function () {_gaq.push(['_trackEvent', 'Our Food Tabs', 'Tab Link Click', 'Tab - Our Values']);});
</script>