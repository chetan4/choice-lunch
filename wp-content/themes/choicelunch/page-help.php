<?php
/*
Template Name: Help
*/
?>
<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span8 offset2 intro">
              <?php
                $pid = ($post->post_parent?$post->post_parent:$post->ID);
                $page = get_page($pid);
              ?>
              <h1><?php echo ( function_exists('the_subheading') && get_the_subheading($pid) != '' ? get_the_subheading($pid) : get_the_title($pid) ); ?></h1>
              <?php echo $page->post_content; ?>
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>
      </section>

      <!-- Support -->
      <section>
        <div class="container">
          <div class="row">
            <div class="span10 offset1">
			  
			  <div id="getsat-widget-6456"></div>
			
              <h3 class="">Not finding what you're looking for?</h3>
              <div class="row contact_btns">
                <div class="span4"><a class="btn btn-medium btn-green btn-block" href="mailto:customerservice@choicelunch.com"><i class="icon-envelope-alt"></i> Email Us</a></div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>
<script type="text/javascript">
    if (typeof GSFN !== "undefined") { GSFN.loadWidget(6456, { "containerId": "getsat-widget-6456" }); }
</script>