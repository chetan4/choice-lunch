<?php get_header(); ?>

    <div id="main">

      <section>
        <div class="container">
          <div class="row">
            <div class="span8 content-area">
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="row post">
                <div class="span1 hidden-phone">
                  <div class="date">
                    <div class="month"><?php the_time( 'M' ); ?></div>
                    <div class="day"><?php the_time( 'j' ); ?></div>
                    <div class="year"><?php the_time( 'Y' ); ?></div>
                  </div>
                </div>
                <div class="span7">
                  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                  <div class="meta">By <?php the_author_posts_link() ?><span class="hidden-desktop hidden-tablet"> on <?php the_time( 'M j, Y' ); ?></span></div>
                  <div class="row">
                    <div class="span4">
                      <div class="entry">
                        <?php the_excerpt(); ?>
                      </div>
                      <div class="meta">Posted in <?php the_category(', '); ?> |  <a href="<?php the_permalink(); ?>#comments"><?php comments_number( 'no comments yet', '1 comment', '% responses' ); ?></a></div>
                      <div class="continue_reading"><a class="btn btn-green r_arrow" href="<?php the_permalink(); ?>">Continue Reading</a></div>
                      <div class="socials" style="margin-top: 10px;">
                        <span class='st_facebook_hcount' displayText='Facebook' st_url="<?php the_permalink(); ?>"></span>
                        <span class='st_twitter_hcount' displayText='Tweet' st_url="<?php the_permalink(); ?>"></span>
                        <span class='st_email_hcount' displayText='Email' st_url="<?php the_permalink(); ?>"></span>
                      </div>
                    </div>
                    <div class="span2 post-thumbnail hidden-phone">
                      <?php if ( has_post_thumbnail() ) : ?>
                      <a href="<?php the_permalink(); ?>"><?php $src = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?><img class="round-border" src="<?php echo get_bloginfo( 'template_directory' ) . '/mk_thumb.php?src=' . $src . '&h=160&w=160'; ?>" alt="<?php the_title_attribute(); ?>" title="<?php the_title_attribute(); ?>" /></a>
                      </a>
                      <?php endif; ?>
                    </div>
                    <div class="span1">&nbsp;</div>
                  </div>
                </div>
              </div>
              <?php endwhile; else: ?>
              <div class="alert alert-error">
                <h4>Oh Snap!</h4>
                <p>There aren't any blog posts</p>
              </div>
              <?php endif; ?>
            
              <?php
                if( function_exists( 'wp_paginate_comments' ) ) {
                  wp_paginate();
                }
                else {
                  ?> <div class="pagination"><p><?php posts_nav_link(); ?></p></div> <?php
                }
              ?>
            </div>
            <div class="span4 sidebar hidden-phone">
              <div class="row">
                <div class="span4">
                  <?php get_sidebar(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>