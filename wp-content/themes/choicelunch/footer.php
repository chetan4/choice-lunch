		<footer>
			<div class="container">
				<div class="row hidden-phone hidden-tablet">
			      <div class="span2">
			      	<?php wp_nav_menu( array( 'theme_location' => 'footer1', 'depth' => 1, 'container' => false, 'container_class' => '', 'menu_class' => '', 'items_wrap' => '<ul id="%1$s" class="nav">%3$s</ul>' ) ); ?>
              	  </div>
			      <div class="span2">
			      	<?php wp_nav_menu( array( 'theme_location' => 'footer2', 'depth' => 1, 'container' => false, 'container_class' => '', 'menu_class' => '', 'items_wrap' => '<ul id="%1$s" class="nav">%3$s</ul>' ) ); ?>
              	  </div>
              	  <div class="span5" style="margin-top: 0px; font-size: 11px; color: #a68d5a">
              	  		<strong>Privacy Policy: </strong>Your privacy is important to Choicelunch. In setting up your account, you share information about yourself and your family.Choicelunch does not share any of our customer’s information with any outside parties or vendors. Also, we have put in place appropriate electronic, physical, and managerial procedures to safeguard the information that we collect to ensure our customers’ privacy. [<a href="/privacy/">more info</a>]
			    	</div>
			      <div class="span3 copyright">
			      	<p><img src="<?php bloginfo( 'template_url' ); ?>/img/choicelunch-apple-white.png" alt="" /></p>
			      	<p>Copyright &copy; <?php echo date( 'Y' ); ?> Choice Foodservices, Inc.<br />All rights reserved.<br />Patents pending.</p>
			      </div>
			    </div>
			    <div class="row hidden-desktop">
			    	<div class="span12 copyright">
			    		<p><img src="<?php bloginfo( 'template_url' ); ?>/img/choicelunch-apple-white.png" alt="" /></p>
				      	<p>Copyright &copy; <?php echo date( 'Y' ); ?> Choice Foodservices, Inc. All rights reserved. Patents pending.</p>
				    	<a class="tothetop" href=""><i class="icon-circle-arrow-up"></i><br />Back to the top</a>
			    	</div>
			    </div>
			    
			</div>
		</footer>

		<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=440410519343281";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>

	    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	    <script>window.jQuery || document.write('<script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/jquery-1.7.2.min.js"><\/script>')</script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/jquery.fitvids.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/jquery.fittext.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/jquery.scrollTo.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/jquery.validate.min.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/jquery.equalHeights.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/bootstrap/bootstrap-carousel.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/bootstrap/bootstrap-dropdown.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/bootstrap/bootstrap-scrollspy.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/bootstrap/bootstrap-collapse.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/bootstrap/bootstrap-tooltip.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/jquery.mousewheel.min.js"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/fancybox/jquery.fancybox.js?v=2.1.5"></script>
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
	    <script src="http://a.vimeocdn.com/js/froogaloop2.min.js"></script> <!-- for vimeo api -->
	    <script src="http://www.google.com/jsapi" type="text/javascript"></script>
	    <script type="text/javascript">google.load("swfobject", "2.2");</script> 
	    <script src="<?php bloginfo( 'template_directory' ); ?>/js/script.js"></script>
			<script type="text/javascript">

			  var _gaq = _gaq || [];
			  
			  _gaq.push(['_setAccount', 'UA-5093214-1']);
			  _gaq.push(['_setDomainName', '.choicelunch.com']);
			  _gaq.push(['_trackPageview']);

			  (function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			  })();

			  <?php if ( is_search() ) : ?>
		        _gaq.push(['_trackPageview', '/?s=<?php echo $_GET['s']; ?>']);
			  <?php endif; ?>
			</script>

		<?php wp_footer(); ?>

<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0024/6319.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
<script type="text/javascript">
adroll_adv_id = "EYHS3BSFYRDO7PQXGAGUKV";
adroll_pix_id = "NQZ3FOJKANDYNE76XOS4I3";
(function () {
var oldonload = window.onload;
window.onload = function(){
   __adroll_loaded=true;
   var scr = document.createElement("script");
   var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
   scr.setAttribute('async', 'true');
   scr.type = "text/javascript";
   scr.src = host + "/j/roundtrip.js";
   ((document.getElementsByTagName('head') || [null])[0] ||
    document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
   if(oldonload){oldonload()}};
}());
</script>
	</body>
</html>
