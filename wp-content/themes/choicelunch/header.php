<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
    <meta property="fb:admins" content="1074114019" />
    <meta property="fb:app_id" content="440410519343281" />
    <meta property="og:title" content="<?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php the_permalink(); ?>" />
    <meta property="og:image" content="<?php bloginfo('template_directory'); ?>/img/choicelunch-apple-white.png" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.ico" type="image/x-icon" />

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Asap:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_directory' ); ?>/js/libs/fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_directory' ); ?>/js/libs/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
    
    <script src="<?php bloginfo( 'template_directory' ); ?>/js/libs/modernizr-2.5.3-respond-1.1.0.min.js"></script>
    <style type="text/css">
        .ltIE7 { display: none; }
        .ltIE8 { display: none; }
        .lt-ie7 #login { visibility:hidden; display:none; }
        .lt-ie7 .ltIE7 { display: block; }
        .lt-ie8 .ltIE8 { display: block; }
    </style>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?> data-spy="scroll">
    <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
	<script type="text/javascript">
	  var _kiq = _kiq || [];
	  (function(){
		setTimeout(function(){
		var d = document, f = d.getElementsByTagName('script')[0], s = d.createElement('script'); s.type = 'text/javascript'; 
		s.async = true; s.src = '//s3.amazonaws.com/ki.js/50265/ayc.js'; f.parentNode.insertBefore(s, f);
		}, 1);
	  })();
	</script>
	
    <header>
      <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </a>
            <a class="brand" href="<?php bloginfo( 'url' ); ?>" onClick="_gaq.push(['_trackEvent', 'Header Nav', 'Logo Click', 'Choicelunch Logo']);"><img src="<?php bloginfo( 'template_directory' ); ?>/img/choicelunch-apple-white-header.png" alt="" title="" /></a>
            <div class="nav-collapse">
              <?php wp_nav_menu( array( 'theme_location' => 'primary', 'depth' => 1, 'container' => false, 'container_class' => '', 'menu_class' => '', 'items_wrap' => '<ul id="%1$s" class="nav">%3$s</ul>' ) ); ?>
            </div><!--/.nav-collapse -->
            <ul id="menu-secondary" class="secondary-nav pull-right hidden-phone hidden-tablet">
            	<li class="btn btn-orange light menu-item menu-item-type-custom menu-item-object-custom">
            		<a href="https://order.choicelunch.com/Signup/CreateAccount.aspx" onclick="_gaq.push(['_trackEvent', 'Header Nav', 'Nav Button Click', 'Sign Up']);">Sign Up</a>
            	</li>

            	<li class="dropdown btn btn-orange menu-item menu-item-type-custom menu-item-object-custom" onclick="_gaq.push(['_trackEvent', 'Header Nav', 'Nav Button Click', 'Sign In Order Popover']);" >
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >Sign In</a>
                  <!-- <ul class="dropdown-menu"> -->
                  <div class="dropdown-menu">
                    <form id="signin-form" class="signin">
                        <label id="general-error" class="error" style="display:none;" ></label>
                      <div class="form-group">
                        <input type="text" required class="form-control" name="username" autocomplete="off" placeholder="username">
                      </div>
                      <div class="form-group">
                        <input type="password" required class="form-control" name="password" autocomplete="off" placeholder="password">
                      </div>
                      <button type="submit" class="btn btn-orange btn-block text-capitalize">Sign In</button>
                    </form>
                    <div class="login-meta">
                        <p><a id="RegisterLink2" href="https://order.choicelunch.com/Account/Signup/CreateAccount.aspx">Create new account</a></p>
                        <p><a id="forgotLink" href="https://order.choicelunch.com/Account/ForgotPassword.aspx">Forgot password</a></p>
                    </div>
                  </div>
                </li>
            </ul>
          </div>
        </div>
      </div>
    </header>