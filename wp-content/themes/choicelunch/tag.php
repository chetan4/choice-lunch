<?php get_header(); ?>

    <div id="main">

      <section>
        <div class="container">
          <div class="row">
            <div class="span12">
              <h1><?php printf( __( 'Tag Archives: %s', 'twentyeleven' ), '<span>' . single_tag_title( '', false ) . '</span>' ); ?></h1>
            </div>
          </div>
          <div class="row">
            <div class="span8 content-area">
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="row post">
                <div class="span8">
                  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                  <div class="meta">By <?php the_author_posts_link() ?> on <?php the_time( 'M j Y' ); ?></div>
                  <div class="row">
                    <div class="span8">
                      <div class="entry">
                        <?php the_excerpt(); ?>
                      </div>
                      <div class="meta">Posted in <?php the_category(', '); ?> |  <a href="<?php the_permalink(); ?>#comments"><?php comments_number( 'no comments yet', '1 comment', '% responses' ); ?></a></div>
                    </div>
                  </div>
                </div>
              </div>
              <?php endwhile; else: ?>
              <div class="alert alert-error">
                <h4>Oh Snap!</h4>
                <p>There aren't any blog posts</p>
              </div>
              <?php endif; ?>
            </div>
            <div class="span4 sidebar">
              <div class="row">
                <div class="span4">
                  <?php get_sidebar(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>