<div class="post-comments">
	<?php
	// Do not delete these lines
	  if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	    die ('Please do not load this page directly. Thanks!');

	  if ( post_password_required() ) { ?>
	  	<div class="alert alert-info"><?php _e("This post is password protected. Enter the password to view comments.","choicelunch"); ?></div>
	  <?php
	    return;
	  }
	?>

	<!-- You can start editing here. -->

	<?php if ( have_comments() ) : ?>
		<?php if ( ! empty($comments_by_type['comment']) ) : ?>
		<h3 id="comments"><?php comments_number('<span>' . __("No","choicelunch") . '</span> ' . __("Responses","choicelunch") . '', '<span>' . __("One","choicelunch") . '</span> ' . __("Response","choicelunch") . '', '<span>%</span> ' . __("Responses","choicelunch") );?> <?php _e("to","choicelunch"); ?> &#8220;<?php the_title(); ?>&#8221;</h3>

		<div class="nav">
			<ul class="nav nav-pills">
		  		<li><?php previous_comments_link( __("Older comments","choicelunch") ) ?></li>
		  		<li><?php next_comments_link( __("Newer comments","choicelunch") ) ?></li>
		 	</ul>
		</div>
		
		<ol class="commentlist">
			<?php wp_list_comments('type=comment&callback=choicelunch_comments'); ?>
		</ol>
		
		<?php endif; ?>
		
		<?php if ( ! empty($comments_by_type['pings']) ) : ?>
			<h3 id="pings">Trackbacks/Pingbacks</h3>
			
			<ol class="pinglist">
				<?php wp_list_comments('type=pings&callback=list_pings'); ?>
			</ol>
		<?php endif; ?>
		
		<div class="nav">
			<ul class="nav nav-pills">
		  		<li><?php previous_comments_link( __("Older comments","choicelunch") ) ?></li>
		  		<li><?php next_comments_link( __("Newer comments","choicelunch") ) ?></li>
		 	</ul>
		</div>
	  
		<?php else : // this is displayed if there are no comments so far ?>

		<?php if ( comments_open() ) : ?>
	    	<!-- If comments are open, but there are no comments. -->

		<?php else : // comments are closed 
		?>
		
		<?php
			$suppress_comments_message = of_get_option('suppress_comments_message');

			if (is_page() && $suppress_comments_message) :
		?>
				
			<?php else : ?>
			
				<!-- If comments are closed. -->
				<p class="alert alert-info"><?php _e("Comments are closed","choicelunch"); ?>.</p>
				
			<?php endif; ?>

		<?php endif; ?>

	<?php endif; ?>


	<?php if ( comments_open() ) : ?>

	<div id="comments-form" class="comments-form">

		<h3 id="comment-form-title"><?php comment_form_title( __("Leave a Reply","choicelunch"), __("Leave a Reply to","choicelunch") . ' %s' ); ?></h3>

		<div id="cancel-comment-reply">
			<p class="small"><?php cancel_comment_reply_link( __("Cancel","choicelunch") ); ?></p>
		</div>

		<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
	  	<div class="help">
	  		<p><?php _e("You must be","choicelunch"); ?> <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e("logged in","choicelunch"); ?></a> <?php _e("to post a comment","choicelunch"); ?>.</p>
	  	</div>
		<?php else : ?>

		<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" class="form-vertical" id="commentform">

		<?php if ( is_user_logged_in() ) : ?>

		<p class="comments-logged-in-as"><?php _e("Logged in as","choicelunch"); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e("Log out of this account","choicelunch"); ?>"><?php _e("Log out","choicelunch"); ?> &raquo;</a></p>

		<?php else : ?>
		
		<div class="control-group">
		  <label for="author"><?php _e("Name","choicelunch"); ?> <?php if ($req) echo "(required)"; ?></label>
		  <div class="input-prepend">
		  	<span class="add-on"><i class="icon-user"></i></span><input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" placeholder="<?php _e("Your Name","choicelunch"); ?>" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> />
		  </div>
	  	</div>
		
		<div class="control-group">
		  <label for="email"><?php _e("Mail","choicelunch"); ?> <?php if ($req) echo "(required)"; ?></label>
		  <div class="input-prepend">
		  	<span class="add-on"><i class="icon-envelope"></i></span><input type="email" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" placeholder="<?php _e("Your Email","choicelunch"); ?>" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> />
		  	<span class="help-inline">(<?php _e("will not be published","choicelunch"); ?>)</span>
		  </div>
	  	</div>
		
		<div class="control-group">
		  <label for="url"><?php _e("Website","choicelunch"); ?></label>
		  <div class="input-prepend">
		  <span class="add-on"><i class="icon-home"></i></span><input type="url" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" placeholder="<?php _e("Your Website","choicelunch"); ?>" tabindex="3" />
		  </div>
	  	</div>

		<?php endif; ?>
		
		<div class="control-group">
			<label>Comment</label>
			<textarea name="comment" id="comment" placeholder="<?php _e("Your Comment Here…","choicelunch"); ?>" tabindex="4"></textarea>
		</div>
		
		<div class="">
		  <input class="btn" name="submit" type="submit" id="submit" tabindex="5" value="<?php _e("Submit Comment","choicelunch"); ?>" />
		  <?php comment_id_fields(); ?>
		</div>
		
		<?php 
			//comment_form();
			do_action('comment_form()', $post->ID); 
		
		?>
		
		</form>
		
		<?php endif; // If registration required and not logged in ?>
	</div>

	<?php endif; // if you delete this the sky will fall on your head ?>
</div>