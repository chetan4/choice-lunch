<div class="row">
  <div class="span4">
    <?php get_search_form( true ); ?>
  </div>
</div>

<div class="row">
  <div class="span4">
    <div class="title">Subscribe</div>
    <ul class="unstyled">
      <li><a href="/feed" onclick="_gaq.push(['_trackEvent', 'Blog', 'Subscribe Link Click', 'RSS Link']);"><i class="icon-rss"></i> RSS</a></li>
      <li><a href="http://feedburner.google.com/fb/a/mailverify?uri=choicelunch&loc=en_US" onclick="_gaq.push(['_trackEvent', 'Blog', 'Subscribe Link Click', 'Email Link']);"><i class="icon-envelope-alt"></i> Email</a></li>
    </ul>
  </div>
</div>

<div class="row">
  <div class="span4">
    <div class="title">Categories</div>
    <ul class="unstyled">
		<?php wp_list_categories('orderby=name&title_li='); ?> 
	</ul>
  </div>
</div>

<div class="row">
  <div class="span4">
    <div class="title">Popular Now</div>
    <?php if (function_exists('popular_posts')) popular_posts(); ?>
  </div>
</div>

<div class="row">
    <div class="span4">
      <div class="fb-like-box" style="width: 100%" data-href="https://facebook.com/choicelunch" data-show-faces="true" data-stream="true" data-header="true"></div>
    </div>
  </div>
