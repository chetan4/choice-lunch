<?php
/*
Template Name: Contact
*/
?>
<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span8 offset2 intro">
              <?php
                $pid = ($post->post_parent?$post->post_parent:$post->ID);
                $page = get_page($pid);
              ?>
              <h1><?php echo ( function_exists('the_subheading') && get_the_subheading($pid) != '' ? get_the_subheading($pid) : get_the_title($pid) ); ?></h1>
              <?php echo $page->post_content; ?>
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>
      </section>

      <!-- Support -->
      <section>
        <div class="container">
          <div class="row">
            <div class="span8 offset2">
              <h3 class="">Simply want to chat?</h3>
              <p> For the fastest response, send us an email. You can also find us online on Facebook and Twitter.</p>
              <div class="row contact_btns">
                <div class="span4"><a class="btn btn-medium btn-green btn-block" href="mailto:customerservice@choicelunch.com"><i class="icon-envelope-alt"></i> Email</a></div>
                <div class="span2"><a class="btn btn-medium btn-green btn-block" href="https://www.facebook.com/choicelunch"><i class="icon-facebook"></i> Facebook</a></div>
                <div class="span2"><a class="btn btn-medium btn-green btn-block" href="https://twitter.com/choicelunch"><i class="icon-twitter"></i> Twitter</a></div>
              </div>
            </div>
          </div>

          <div class="row" style="margin-top: 50px;">
            <div class="span4 offset2">
              <h3 class="">Other Ways To Get In Touch</h3>
              <!-- <h4>Email</h4>
              <p>For fastest response, email us at <a href="mailto:customerservice@choicelunch.com">customerservice@choicelunch.com</a></p> -->
              <h4>Snail Mail:</h4>
              <address>
                Choicelunch<br />
                569 San Ramon Valley Boulevard<br />
                Danville, CA 94526<br />
              </address>
              <h4>We'd love to hear your voice:</h4>
              <p>We're available Monday - Friday, 8 AM - 4 PM</p>
              <ul>
                <li>(855) GO-LUNCH (Toll-Free)</li>
                <li>(855) 465-8624</li>
                <li>(925) 867-4660 (Fax)</li>
              </ul>
            </div>
            <div class="span4">
              <h3 class="mobile-top-padding">Join Our Team</h3>
              <p>We’re always looking for talented people who share our passion for food and kids. Tell us a story. Shoot us a note.  Or send us your favorite recipe. We promise to get in touch if we think it’s a good fit.</p>
              <p><a class="btn btn-medium btn-green btn-block" href="https://www.jobscore.com/jobs2/choicelunch" target="_blank"><i class="icon-inbox"></i> Send us your Resume</a></p>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>