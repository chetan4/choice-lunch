<?php
/*
Template Name: How It Works
*/
?>
<?php get_header(); ?>

    <div id="main">

      <section id="welcome">
        <div class="container">
          <div class="row">
            <div class="span8 offset2 intro">
              <?php
                $pid = ($post->post_parent?$post->post_parent:$post->ID);
                $page = get_page($pid);
              ?>
              <h1><?php echo ( function_exists('the_subheading') && get_the_subheading($pid) != '' ? get_the_subheading($pid) : get_the_title($pid) ); ?></h1>
              <?php echo $page->post_content; ?>
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>
      </section>

      <!-- How It Works -->
      <section id="how_it_works">
        <div class="container">
          
          <div class="row">
            <div class="span12">
              <h2>How It Works</h2>

              <div class="infographic">
                <div class="step step1">
                  <div class="icon"><div class="inner"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_hiw_arrow.png" alt="" title="" /></div></div>
                  <h3>Register/Sign Up</h3>
                  <div class="detail">Getting started with Choicelunch is easy. Just enter your school code, set up your profile and you're ready to go.</div>
                </div>

                <div class="step step2">
                  <div class="icon"><div class="inner"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_hiw_display.png" alt="" title="" /></div></div>
                  <h3>Choose Lunches</h3>
                  <div class="detail">Browse our selection of 16 daily delicious entree options and plan lunches days or weeks in advance, tailored to your child's tastes and nutritional needs.</div>
                </div>

                <div class="step step3">
                  <div class="icon"><div class="inner"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_hiw_food.png" alt="" title="" /></div></div>
                  <h3>Make The Food</h3>
                  <div class="detail">Choicelunch chefs get to work creating your lunches using healthy, fresh ingredients and kid-approved recipes.</div>
                </div>

                <div class="step step4">
                  <div class="icon"><div class="inner"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_hiw_truck.png" alt="" title="" /></div></div>
                  <h3>Deliver The Food</h3>
                  <div class="detail">The trucks are loaded up and sent on their way to your child's school for same-day delivery. We track them by satellites to make sure they get there on time.</div>
                </div>

                <div class="step step5">
                  <div class="icon"><div class="inner"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_hiw_sides.png" alt="" title="" /></div></div>
                  <h3>Choose Your Sides</h3>
                  <div class="detail">Kids get to choose between a great selection of healthy sides and drinks in the lunch line when they get their daily entree.</div>
                </div>

                <div class="step step6">
                  <div class="icon"><div class="inner"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_hiw_smiles.png" alt="" title="" /></div></div>
                  <h3>Yum!</h3>
                  <div class="detail">Happy, healthy kids = happy parents. Just repeat the process for added happiness!</div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </section>

      <!-- Features -->
      <section id="features">
        <div class="container">
          <div class="row">
            <div class="span12">
              <div class="inner">
                <div class="feature green">
                  <div class="icon"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_rate.png" alt="" title="" /></div>
                  <h3>Rate Lunches</h3>
                  <div class="detail">We're constantly improving our menu based on feedback from parents like you.</div>
                </div>
                <div class="feature orange">
                  <div class="icon"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_check.png" alt="" title="" /></div>
                  <h3>EasyOrder</h3>
                  <div class="detail">A fast, easy way to schedule lunches based on your kids favs. <a href="#easyorder" onclick="_gaq.push(['_trackEvent', 'Inline Link', 'Link Click', 'EasyOrder - Learn More']);">Learn More</a></div>
                </div>
                <div class="feature blue">
                  <div class="icon"><img src="<?php bloginfo( 'template_directory' ); ?>/img/icn_iphone.png" alt="" title="" /></div>
                  <h3>Mobile Ordering</h3>
                  <div class="detail">Order lunches on the go using our iPhone app! <a href="#app" onclick="_gaq.push(['_trackEvent', 'Inline Link', 'Link Click', 'Mobile Ordering - Learn More']);">Learn More</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
          
      <!-- buttons -->
      <section id="calltoaction" class="shadow_bottom">
        <div class="container">
          <div class="row">
            <div class="span4 offset4 taligncenter callout">
              <p class="hidden-phone"><a class="btn btn-large btn-orange btn-block" href="https://order.choicelunch.com" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Sign In / Order']);">Sign In / Order</a></p>
              <p class="hidden-desktop hidden-tablet"><a class="btn btn-large btn-orange btn-block" href="https://order.choicelunch.com" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Sign In / Order']);">Sign In / Order</a></p>
              <p class="hidden-phone"><a class="btn btn-green btn-block" href="http://order.choicelunch.com/demo" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Button Click', 'Try a Live Demo']);">Try a Live Demo</a></p>
              <h3>Taste the Awesome<sup>&reg;</sup></h3>
              <p><a href="https://order.choicelunch.com/Account/Registration/RegistrationCode.aspx" onclick="_gaq.push(['_trackEvent', 'Order Now Module', 'Link Click', 'Find out how...']);">Find out how you can bring Choicelunch to your school, pre-school or summer camp.</a></p>
            </div>
          </div>
        </div>
      </section>

      <!-- Choicelunch Advantage -->
      <section id="choicelunch_advantage">
        <div class="container">
          <div class="row" id="easyorder">
            <div class="span7 pull-right"><img class="round-border" src="<?php bloginfo( 'template_directory' ); ?>/img/how-it-works/easyorder.jpg" alt="" title="" /></div>
            <div class="span5 pull-left">
              <h3>EasyOrder</h3>
              <p>EasyOrder is our new convenient and super simple way to order your school lunches. For parents who know which entrees their kid likes best, EasyOrder learns your kid’s preferences, anticipates their tastes and then designs the optimum lunch plan.</p>
              <p>How does it work? Simply rate your kid’s favorite entrees and EasyOrder creates a custom calendar for every month. And with our new loyalty rewards program, for every 20 lunches you order, the next one’s on us. How easy is that?</p>
            </div>
          </div>
          <div class="row">
            <div class="span7 pull-left"><img class="round-border" src="<?php bloginfo( 'template_directory' ); ?>/img/how-it-works/not-just-entrees-alt.jpg" alt="" title="" /></div>
            <div class="span5 pull-right">
              <h3>Not Just Entrees</h3>
              <p>Choicelunch is the main course and so much more. After you select your child’s entrée, they get to choose their own snack, fruit or vegetable, and a drink all conveniently located in the lunch line. The variety is truly awesome. From apple slices, baby carrots and edamame, to Clif Organic Z Fruit and Popchips. Our sides tend to make kids - and parents - happy campers.</p>
              <p><a href="<?php echo get_page_link(9); ?>" onclick="_gaq.push(['_trackEvent', 'Inline Link', 'Link Click', 'Learn more about our food']);">Learn more about our food &raquo;</a></p>
            </div>
          </div>
          <div class="row">
            <div class="span7 pull-right"><img class="round-border" src="<?php bloginfo( 'template_directory' ); ?>/img/how-it-works/info_true_nutritional_value.png" alt="" title="" /></div>
            <div class="span5 pull-left">
              <h3>True Nutritional Value</h3>
              <p>How long would it take you to source real, California ingredients and prepare a healthy, nutritious lunch for your child every day? How much would it cost? Based on our experience, it takes a great deal of time and effort to make a great lunch happen. To put it in perspective, Choicelunch is less expensive than: 1 box of your kid’s favorite whole grain cereal, 1/5th of a pedicure (2 toes!), and 1 Kogi Dog or Power Peach smoothie from Jamba Juice.</p>
              <p>Choicelunch’s true nutritional value doesn’t just come from the real ingredients we use or the variety of healthy sides we offer. Our true value is a carefully designed combination of healthy, nutrient-rich food, easy ordering and simple delivery. That’s the Choicelunch difference.</p>
            </div>
          </div>
          <div class="row">
            <div class="span7 pull-left"><img class="round-border" src="<?php bloginfo( 'template_directory' ); ?>/img/how-it-works/lateorder.jpg" alt="" title="" /></div>
            <div class="span5 pull-right">
              <h3>Late Ordering</h3>
              <p>Forget to order lunch? It happens to the best of us. Now you can order any morning before 6 a.m. and have it arrive the same day.</p>
              <p>Pick from our list of late entrees and we’ll make sure it gets there in time for lunch. Of course your son or daughter still gets to choose their favorite sides and pick a healthy drink. Procrastination never tasted this good!</p>
            </div>
          </div>
          <div class="row" id="app">
            <div class="span5 pull-left">
              <h3>App-A-Licious</h3>
              <p>The Choicelunch mobile app brings ordering school lunch into the 21st century. Now you can order school lunch from pretty much anywhere. Search by entrée. Change or cancel lunches. Filter by allergen. Your child’s next nutritious lunch is only an app away.</p>
              <ul class="videos">
                    <li><a vid="kZkWZevMGHk" id="vid_overview" href="http://youtu.be/kZkWZevMGHk" onclick="_gaq.push(['_trackEvent', 'Video', 'Video Play', 'App-A-Licious Overview']);">Overview</a></li>
                    <li><a vid="Ul0kA-XBHAw" id="vid_orderbycal" href="http://youtu.be/Ul0kA-XBHAw" onclick="_gaq.push(['_trackEvent', 'Video', 'Video Play', 'App-A-Licious Order by Calendar']);">Order by Calendar</a></li>
                    <li><a vid="C_tLV08OWio" id="vid_orderbyentree" href="http://youtu.be/C_tLV08OWio" onclick="_gaq.push(['_trackEvent', 'Video', 'Video Play', 'App-A-Licious Order by Entrée or Favorite']);">Order by Entrée or Favorite</a></li>
                    <li><a vid="vNBlnLObi6A" id="vid_allergens" href="http://youtu.be/vNBlnLObi6A" onclick="_gaq.push(['_trackEvent', 'Video', 'Video Play', 'App-A-Licious Filter by Allergens for Each Child']);">Filter by Allergens for Each Child</a></li>
                    <li><a vid="q5e_4BZoAdo" id="vid_changecancel" href="http://youtu.be/q5e_4BZoAdo" onclick="_gaq.push(['_trackEvent', 'Video', 'Video Play', 'App-A-Licious Change and Cancel Lunches']);">Change and Cancel Lunches</a></li>
                </ul>
            </div>
            <div class="span7 pull-right">
              <div id="iphone" class="iphone">
                <div class="screen" id="iphone_screen"><div class="inner">&#160;</div></div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>