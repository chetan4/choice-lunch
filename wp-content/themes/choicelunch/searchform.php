<form class="form-search" role="search" id="searchform" action="<?php echo home_url( '/' ); ?>" method="get">
	<input class="input search-query span2" type="text" name="s" placeholder="" id="s" value="<?php echo ( isset( $_GET['s'] ) ? $_GET['s'] : '' ); ?>" />
    <input class="btn btn-orange" type="submit" id="searchsubmit" value="Search" />
</form>