<?php get_header(); ?>

    <div id="main">

      <section>
        <div class="container">
          <div class="row">
            <div class="span12 content-area">
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <h1><?php the_title(); ?></h1>
              <?php the_content(); ?>
              <?php endwhile; endif; ?>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>