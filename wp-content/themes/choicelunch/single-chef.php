<?php get_header(); ?>

    <div id="main">

      <section>
        <div class="container">
          <div class="row">
            <div class="span12 content-area">
              <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <div class="row post">
                <div class="span12">
                  <h2><?php the_title(); ?></h2>
                  <div class="entry">
                    <?php the_content(); ?>
                  </div>
                </div>
              </div>
              <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </section>

    </div>

<?php get_footer(); ?>