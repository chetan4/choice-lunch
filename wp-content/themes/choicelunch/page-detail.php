<?php
/*
Template Name: Food Details
 */
?>
<?php get_header(); ?>

    <div id="main" class="food_details">

      <section id="chefs">
        <div class="container">
          <div class="row">
            <div class="span12">
              <h2 id="chefs" class="title_chefs">Our Chefs <i class="icon-caret-right"></i></h2>
              <div class="content chefs">
                <?php
                  $chefs = new WP_Query( array( 'pagename' => 'food/chefs', 'post_type' => 'page', 'posts_per_page' => 1 ) );
                  if ( $chefs->have_posts() ) : while ( $chefs->have_posts() ) : $chefs->the_post();
                    the_content();
                    wp_reset_postdata();
                  endwhile; endif;
                  wp_reset_query();
                ?>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="ingredients">
        <div class="container">
          <div class="row">
            <div class="span12">
              <?php
                $ingredients = new WP_Query( array( 'pagename' => 'food/ingredients', 'post_type' => 'page', 'posts_per_page' => 1 ) );
                if ( $ingredients->have_posts() ) : while ( $ingredients->have_posts() ) : $ingredients->the_post();
                  echo '<h2 id="ingredients" class="title_' . $post->post_name . '">' . get_the_title() . ' <i class="icon-caret-right"></i></h2>';
                  echo '<div class="content ' . $post->post_name . '">';
                  the_content();
                  echo '</div>';
                  wp_reset_postdata();
                endwhile; endif;
                wp_reset_query();
              ?>
            </div>
          </div>
        </div>
      </section>

      <section id="process">
        <div class="container">
          <div class="row">
            <div class="span12">
              <?php
                $process = new WP_Query( array( 'pagename' => 'food/process', 'post_type' => 'page', 'posts_per_page' => 1 ) );
                if ( $process->have_posts() ) : while ( $process->have_posts() ) : $process->the_post();
                  echo '<h2 id="process" class="title_' . $post->post_name . '">' . get_the_title() . ' <i class="icon-caret-right"></i></h2>';
                  echo '<div class="content ' . $post->post_name . '">'; 
                  the_content();
                  echo '</div>';
                  wp_reset_postdata();
                endwhile; endif;
                wp_reset_query();
              ?>
            </div>
          </div>
        </div>
      </section>

      <section id="values">
        <div class="container">
          <div class="row">
            <div class="span12">
              <h2 id="values" class="title_values">Our Values <i class="icon-caret-right"></i></h2>
              <div class=" content values">
                <?php echo get_template_part('part-testimonials'); ?>
              </div>
            </div>
          </div>
        </div>
      </section>




    </div>

<?php get_footer(); ?>