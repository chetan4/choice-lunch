<?php
/*
Plugin Name: WEBDOGS Support
Plugin URI: http://webdogs.com/
Description: Use this Dashboard Widget and contact support@webdogs.com for assistance with updating WordPress or making changes to your website.
Version: 1.0
Author: WEBDOGS
Author URI: http://webdogs.com/
License: WDLv1
*/

// If class WEBDOGS already exists, return and let's not procede. That would cause errors.
if (!class_exists('WEBDOGS')) return;

// The class is useless if we do not start it, when plugins are loaded let's start the class.
add_action ('plugins_loaded', 'WEBDOGS');
function WEBDOGS() { $WEBDOGS = new WEBDOGS; }

class WEBDOGS
{
    function __construct() 
    {
        define( 'WEBDOGS_TITLE', "WEBDOGS Support" );
        define( 'WEBDOGS_SUPPORT', "support@webdogs.com" );

        if(!function_exists('is_plugin_active')) 
        {
            include_once( ABSPATH . 'wp-admin/includes/plugin.php');
        }

        add_action( 'wp_dashboard_setup',                   array(&$this,'webdogs_add_dashboard_widget'         ));
    
        add_action( 'admin_enqueue_scripts',                array(&$this,'webdogs_enqueue_scripts'              ));
    
        add_action( 'wp_ajax_webdogs_result_dashboard',     array(&$this,'webdogs_result_dashboard_callback'    )); 

        add_action( 'wp_ajax_webdogs_reset_dashboard',      array(&$this,'webdogs_reset_dashboard_callback'     )); 

        add_filter( 'the_generator',                        array(&$this,'complete_version_removal'             ));


    }

    /**
     * Register dashboard widget
     * @return void
     */
    function webdogs_add_dashboard_widget() {
        wp_add_dashboard_widget( 'webdogs_support_widget', WEBDOGS_TITLE, array(&$this,'webdogs_dashboard_widget_function' ));
    }

    /**
     * Display widget
     * @return void
     */
    function webdogs_dashboard_widget_function() { 
        include_once('webdogs-support-form.php');
    }

    /**
     * load JS and the data (admin dashboard only)
     * @param  string $hook current page
     * @return void
     */
    function webdogs_enqueue_scripts( $hook ) {

        if( 'index.php' != $hook ) {
            add_action( 'admin_head', array(&$this,'webdogs_dashboard_notification'));
        } else {
            add_action( 'admin_footer', array(&$this,'webdogs_dashboard_javascript'));
        }
    }

    /**
     * Display widget
     * @return void
     */
    function webdogs_dashboard_notification() { ?>
        <script type="text/javascript">
            $j = jQuery; $j().ready(function(){$j('.wrap > h2').parent().prev().after('<div class="update-nag" style="font-family:\'Helvetica Neue\',Helvetica,Arial,sans-serif;color:#FFFFFF;text-align:left;vertical-align:middle;background-color:#666666;border-left-color:#377A9F;padding-top: 3px;padding-bottom: 3px;padding-left: 12px;"><a style="color:#FFFFFF;text-decoration:none;font-weight:bold;text-transform:uppercase;padding-right: 20px;padding-bottom: 6px;border-right: 1px solid #bbb;margin-right: 20px;" title="WEBDOGS" href="http://webdogs.com/" target="_blank"><b><img style="border-width:0px;margin-right:5px;margin-left:0px;vertical-align:middle;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAQAAAC00HvSAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAQjJfA44AAAAHdElNRQfeDBYLLDqnCm9AAAADv0lEQVRIx5VWCU8TQRTe31aQQ9BqlcuYCBRRbpRGDIcWjJVoUJFLBFEUFSUqigoFBQkYwUA0GAyJByB4EVAwSjAIn99Op8tuu21wJu3uvvfm25l3fG8VixJ8RsKKLQhDcKuAilRUox1dcKOTv27+bqEUNmwYJoTmbjxFPXIQLZeFIoHS2xhAC5KxAZh0POf77cI0gkscKEYBMrEDHjgXRnBNgw8A04hhZNAkDOUYwjzWxxLe4yYSxfJavEIaAsCE0BcPEUp1Babl4kXMaUCr/C3jEeJpkYXXOAxTmHY60aLE4IVuD/08kncU0y/q+A4n7RIwhoPwg6nHXQqT8Q368ZKySXG3wLtTmrxBAL2Rh9RgsnnaTdiNWRjHAsJxTNy95YLjOk01n/PxTA8TxuikMsXG4D9UD0zxOij2uqrzVC4lrTgNDaYMD/hwBWajiRoXr9eF+X2dZgZRiMEo/yVMH3YxP5dMYXLgcX+SuFp1kQNqKGtmJgmYdKa8RblkAjGBo9gmUmB91ur0XxGJPejzwDThLG8++0C8wwnGoYQ+syMfF5EnwQ4YrAopHWShUNFDw0SsGdQddHgCK8vO9/0QkklZ5YUGuzuUtaEIjFI/DVwG5UeEkB7GsY9G4fDkzRr3pcIYLUcpO0kfKTYTzwyKUBbJY+wX+/klSSLbYDlDWR6uQonjAdRF3rHMzD3H/WXqHFtA+R9GU70PZyKujzkmbSZuQIk1wIyI9HaSHoyVX4EVOtrr5FUdTCiyVBgrHlPdIMVlwvQCqgwwDpLHXx10h86Lakk0gfQwQBIqleICUYBfEKctiUYvpsguDsbMK4vBT13p1qACAjuL51YDvsIgJ8h3eGcyflNyxOeQzRJGpZZOEgZFlWiU5TfPN0ayenqYu+tLXJIY9DOaMVKHg6kxzORQVN5Q07mKwllEmNB1HDUVfvIsSqcZp1xypixNdVvRJMwVVog5TKuJvI2JYVHcggNlCFX6qaZ5t4m5nflcaSIP417SSLkh0NivHcf5MESgAaLH87RRWmWHB+mYfZJItBCOM8hWfJCZvEg/TdBn+UGbbiMboA+lO5jBm7GTcMbxBNsDQJWwk0XBr8GcISNvZay6fIAmJfMZp5NNIA6mXbOMTSwFaika9/SJnGsEOc/8tSFg880gUIMkhHam2LIEcuuW7OWu23wyzG+zUbjMaNXJ99vIfxmkr3h4QuwgeJ+ovA1838SSewfYz+vYcNPpmRQmQTnpoJd+c+K/PpMsShKptYVgbs57BD4U8CPJovwDRRo5ALFcUX0AAAAldEVYdGRhdGU6Y3JlYXRlADIwMTQtMTItMjJUMTE6NDQ6NTgrMDE6MDBej4ixAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE0LTEyLTIyVDExOjQ0OjU4KzAxOjAwL9IwDQAAAABJRU5ErkJggg==" alt="" width="35" height="35"><span style="text-decoration:none;color:#FFFFFF;vertical-align:middle;">WEBDOGS</span></b></a><p style="color: #D0F2FC; text-decoration: none;display: inline-block; vertical-align: middle; margin: 6px 0 4px;">Please contact <a href="mailto:support@webdogs.com" target="_blank" style="color: #D0F2FC;">support@webdogs.com</a> for assistance with updating WordPress or making changes to your website.</p></div>'); });
        </script><?php 
    } 

    function webdogs_dashboard_javascript() { ?>

        <script type="text/javascript">
            jQuery(function($) {

                $('#webdogs_support_widget').find('.hndle').css({"background-color":"#666", "font-family":"'Helvetica Neue',Helvetica,Arial,sans-serif"}).html('<span style="color:#FFFFFF;text-decoration:none;font-weight:bold;text-transform:uppercase;padding-top: 0;padding-bottom: 0;display: inline-block;position: relative; vertical-align: middle;margin-top: -3px;line-height: 13px;" title="WEBDOGS" href="http://webdogs.com/" target="_blank"><b><img style="border-width:0px;margin-right:5px;margin-left:0px;margin-top: -13px;margin-bottom: 0;display: inline;vertical-align: middle;line-height: 26px;top: 5px;position: relative;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAQAAAC00HvSAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAQjJfA44AAAAHdElNRQfeDBYLLDqnCm9AAAADv0lEQVRIx5VWCU8TQRTe31aQQ9BqlcuYCBRRbpRGDIcWjJVoUJFLBFEUFSUqigoFBQkYwUA0GAyJByB4EVAwSjAIn99Op8tuu21wJu3uvvfm25l3fG8VixJ8RsKKLQhDcKuAilRUox1dcKOTv27+bqEUNmwYJoTmbjxFPXIQLZeFIoHS2xhAC5KxAZh0POf77cI0gkscKEYBMrEDHjgXRnBNgw8A04hhZNAkDOUYwjzWxxLe4yYSxfJavEIaAsCE0BcPEUp1Babl4kXMaUCr/C3jEeJpkYXXOAxTmHY60aLE4IVuD/08kncU0y/q+A4n7RIwhoPwg6nHXQqT8Q368ZKySXG3wLtTmrxBAL2Rh9RgsnnaTdiNWRjHAsJxTNy95YLjOk01n/PxTA8TxuikMsXG4D9UD0zxOij2uqrzVC4lrTgNDaYMD/hwBWajiRoXr9eF+X2dZgZRiMEo/yVMH3YxP5dMYXLgcX+SuFp1kQNqKGtmJgmYdKa8RblkAjGBo9gmUmB91ur0XxGJPejzwDThLG8++0C8wwnGoYQ+syMfF5EnwQ4YrAopHWShUNFDw0SsGdQddHgCK8vO9/0QkklZ5YUGuzuUtaEIjFI/DVwG5UeEkB7GsY9G4fDkzRr3pcIYLUcpO0kfKTYTzwyKUBbJY+wX+/klSSLbYDlDWR6uQonjAdRF3rHMzD3H/WXqHFtA+R9GU70PZyKujzkmbSZuQIk1wIyI9HaSHoyVX4EVOtrr5FUdTCiyVBgrHlPdIMVlwvQCqgwwDpLHXx10h86Lakk0gfQwQBIqleICUYBfEKctiUYvpsguDsbMK4vBT13p1qACAjuL51YDvsIgJ8h3eGcyflNyxOeQzRJGpZZOEgZFlWiU5TfPN0ayenqYu+tLXJIY9DOaMVKHg6kxzORQVN5Q07mKwllEmNB1HDUVfvIsSqcZp1xypixNdVvRJMwVVog5TKuJvI2JYVHcggNlCFX6qaZ5t4m5nflcaSIP417SSLkh0NivHcf5MESgAaLH87RRWmWHB+mYfZJItBCOM8hWfJCZvEg/TdBn+UGbbiMboA+lO5jBm7GTcMbxBNsDQJWwk0XBr8GcISNvZay6fIAmJfMZp5NNIA6mXbOMTSwFaika9/SJnGsEOc/8tSFg880gUIMkhHam2LIEcuuW7OWu23wyzG+zUbjMaNXJ99vIfxmkr3h4QuwgeJ+ovA1838SSewfYz+vYcNPpmRQmQTnpoJd+c+K/PpMsShKptYVgbs57BD4U8CPJovwDRRo5ALFcUX0AAAAldEVYdGRhdGU6Y3JlYXRlADIwMTQtMTItMjJUMTE6NDQ6NTgrMDE6MDBej4ixAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDE0LTEyLTIyVDExOjQ0OjU4KzAxOjAwL9IwDQAAAABJRU5ErkJggg==" alt="" width="26" height="26"><span style="text-decoration:none;color:#FFFFFF;vertical-align:middle;display: inline-block;font-size: 13px;margin-right: 4px;">WEBDOGS </span></b></span><span style="color: #D0F2FC; text-decoration: none;display: inline-block; vertical-align: middle;margin: -1px 0 0 0;padding: 0;font-size: 13px;line-height: 13px;">Support</span></h3>');

                $('#webdogs_support_form').live('submit', function(e) {
                    var username    = $( this ).find('#webdogs_support_form_username').val();
                    var email       = $( this ).find('#webdogs_support_form_email').val();
                    var subject     = $( this ).find('#webdogs_support_form_subject').val();
                    var message     = $( this ).find('#webdogs_support_form_message').val();
                    var data = 
                    {
                        'action'  : 'webdogs_result_dashboard',
                        'username': username,
                        'email'   : email,
                        'subject' : subject,
                        'message' : message
                    };

                    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                    $.post(ajaxurl, data, function(response) 
                    {
                        $intro     = $( "#webdogs_support_intro" ).html( response );
                        $container = $( "#webdogs_support_form_wrapper" ).detach();
                    });
                    e.preventDefault();
                })
                $('#webdogs_support_form_reset').live('click', function(e) {
                    var data = 
                    {
                        'action':   'webdogs_reset_dashboard'
                    };

                    $.post(ajaxurl, data, function(response) 
                    {
                        $('#webdogs_support_widget .inside').html( response );
                    });
                    e.preventDefault();
                })
            });
        </script><?php
    }
    function webdogs_result_dashboard_callback() {

        $site       = get_bloginfo('name');
        $username   = $_POST['username'];
        $email      = $_POST['email'];
        $message    = $_POST['message'];
        $to         = WEBDOGS_SUPPORT;

        $subject = "[".WEBDOGS_TITLE."] ". html_entity_decode( $site ) ." - ".$_POST['subject'];

        $headers  = "From: \"". $username ."\" <". $email .">\r\n"; // $headers .= "Reply-To: WEBDOGS <wordpress@webdogs.com>\r\n"; // $headers .= "Bcc: WEBDOGS@WEBDOGS.solve360.com, david@webdogs.com, sergey@webdogs.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if ( wp_mail($to, $subject, $message, $headers) )
        {   
           echo "<p style='color:#2a8d9d;'><b>Request Sent</b></p> \r\n";
           echo "<p>A ".WEBDOGS_TITLE." agent will respond to your case via email. To submit another request, click the Reset Form button.</p> \r\n";
           echo "<input id='webdogs_support_form_reset' type='button' class='button' value='Reset Form' /> \r\n";
        } 
        else 
        {   
           echo "<p style='color:#2a8d9d;'><b>Error sending</b></p> \r\n";
           echo "<p>Something went wrong. Please, use your email service to notify <a href='mailto:support@webdogs.com' target='_blank' style='color: #D0F2FC;'>support@webdogs.com</a>.</p> \r\n";
        }
        die();
    }

    function webdogs_reset_dashboard_callback() {
        include_once('webdogs-support-form.php');
        die();
    }

    // remove version info from head and feeds
    function complete_version_removal() {
       return '';
    }

}