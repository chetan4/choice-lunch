<?php 

$current_user = wp_get_current_user();


?>

<div style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;text-size-adjust:100%;-webkit-text-size-adjust:100%;min-width:100%;width:auto;position:relative;" id="webdogs_support_intro">
    <p>Contact support for assistance with updating WordPress or making changes to your website.</p>
</div>
<div id="webdogs_support_form_wrapper">
    <form id="webdogs_support_form" action="" style="font-family:'Helvetica Neue',Helvetica,Arial,sans-serif;text-size-adjust:100%;-webkit-text-size-adjust:100%;min-width:100%;width:auto;position:relative;">
        <label for="webdogs_support_form_username" style="display:block;width:28%;text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#2a8d9d;float:left;margin-right:2%"><b>Name</b> <br>
        <input type="text" style="width:100%" name="webdogs_support_form_username" id="webdogs_support_form_username" value="<?php echo $current_user->display_name; ?>"></label>
        <label for="webdogs_support_form_email" style="display:block;width:28%;text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#2a8d9d;float:left;margin-right:0"><b>Email</b> <br>
        <input type="text" style="width:100%" name="webdogs_support_form_email" id="webdogs_support_form_email" value="<?php echo $current_user->user_email; ?>"></label><br clear="left">
        <label for="webdogs_support_form_subject" style="display:block;margin-top:0.5em;width:100%;text-size-adjust:100%;-webkit-text-size-adjust:100%;color:#2a8d9d;margin-right:0"><b>Subject</b> <br>
        <input type="text" style="width:100%" name="webdogs_support_form_subject" id="webdogs_support_form_subject" value=""></label>
        <label for="webdogs_support_form_message" style="display:block;margin-top:0.5em;text-size-adjust:100%;color:#2a8d9d;-webkit-text-size-adjust:100%;"><b>Message</b> <br>
        <textarea type="textarea" rows="4" style="width:100%" name="webdogs_support_form_message" id="webdogs_support_form_message"></textarea></label><br>
        <input type="submit" class="button" value="Send Request">
    </form>
</div>