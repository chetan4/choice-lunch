<?php
if ( ! defined( 'BASEPATH' ) ) { exit; }

/**
 * Timestamp for activity
 * 
 * @return  string  W3C formated date/time: 2005-08-15T15:52:01+00:00
 *                  
 **/
function now_time()
{ 
    return '['.date(DATE_W3C).']'; 
}

/**
 * Smart GET function
 * Return validated content for a specific key in $_GET or $_POST;
 *
 * @example GET('page'); 
 *
 * @param   string  $name      label for URL param
 * @param   mixed   $value     default value to return on null
 * @return  mixed   $content   value or false if is null or bad input
 *
 **/
function GET( $name=NULL, $value=false ) 
{
    $content = 
    (!empty($_GET[$name]) ? 
        trim($_GET[$name]) : 

        (!empty($_POST[$name]) ? 
            trim($_POST[$name]) : 

            (!empty($value) && !is_array($value) ? 
                trim($value) : false 
            )
        )
    );
    $options = null;
    if(is_numeric($content))
        return preg_replace("@([^0-9])@Ui", "", $content);
    else if(is_bool($content))
        return ($content?true:false);
    else if(is_float($content))
        return preg_replace("@([^0-9\,\.\+\-])@Ui", "", $content);
    else if(is_string($content))
    {
        if(filter_var ($content, FILTER_VALIDATE_URL))
            return $content;
        else if(filter_var ($content, FILTER_VALIDATE_EMAIL))
            return $content;
        else if(filter_var ($content, FILTER_VALIDATE_IP))
            return $content;
        else if(filter_var ($content, FILTER_VALIDATE_FLOAT))
            return $content;
        else
            return preg_replace("@([^a-zA-Z0-9\+\-\_\*\@\$\!\;\,\.\?\#\:\=\%\/\ ]+)@Ui", "", $content);
    }
    else false;
}

/**
 * Discount message output from matched code
 * 
 * @return  string  Discount message or DEBUG
 *                  
 **/
function discount_message( $message = "%s%%", $discountKey = array() ) {
    print_r((DEBUG)?$discountKey:null);

    if ( false === ( $codes = GET('code') ) ) {
        return (DEBUG)?"nocode\n":'';
    }
    $codes = explode( ',', $codes );

    foreach ( $codes as $code ) {
        if ( false === ( $discount = get_discount( $code, $discountKey ) ) ) {
            continue;
        } else {
            return sprintf( $message, $discount );
        }
    }
    return (DEBUG)?"nomatch\n":'';
}

/**
 * Get a discount value for matched code
 * 
 * @return  mixed  Discount value or DEBUG
 *                  
 **/
function get_discount( $code = null, $discountKey = array() ) {
    if ( $code ) {
        foreach ( $discountKey as $discount => $codeSet ) {
            echo (DEBUG)?$code.": ".implode(', ', $codeSet)."\n":'';
            if ( in_array( $code, $codeSet ) ) {
                return intval( $discount );
            }
        }
    }
    else {
        echo (DEBUG)?"nocode\n":'';
    }
    return false;
}

?>