<?php 
/**
 * Unlimited Choicelunch
 *
 *
 * @param string  $codes 
 *                Comma separated school code values from 'code' parameter in URL. 
 *
 * @param string  $message 
 *                Formatted content for the discount message. 
 *
 * @param array   $discountKey 
 *                School code sets indexed by the discount percentage value from 'data/discountKey.json'. 
 *
 **/

define('BASEPATH', realpath(dirname(__FILE__)) );
define('DEBUG', ! empty( $_GET['debug'] ) );

require BASEPATH.'/include/functions.php';

$codes        = null;
$message      = '<span id="discountdisplay" class="price"> - savings of over %s%%</span>';
$discountKey  = json_decode( utf8_encode( file_get_contents( 'data/discountKey.json' ) ), true ); 

?><!DOCTYPE html>
<html lang="en">

<head>
    <script src="//cdn.optimizely.com/js/2218881047.js"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Unlimited Choicelunch</title>
    
    <meta property="og:image" content="//choicelunch.com/wp-content/themes/choicelunch/img/choicelunch-apple-white.png" />
    <link rel="icon" href="//choicelunch.com/wp-content/themes/choicelunch/img/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="//choicelunch.com/wp-content/themes/choicelunch/img/favicon.ico" type="image/x-icon" />

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/main.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='//fonts.googleapis.com/css?family=Nunito:400,700,300' rel='stylesheet' type='text/css'>
    <link href="//fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <a class="navbar-brand" href="#page-top"><img src="img/choicelunch_logo_svg-01.png">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row headerrow">
                <div class="col-lg-12">
                    <div class="intro-text">
                        <span class="name">UNLIMITED CHOICELUNCH</span>
                        <hr class="star-light">
                        <span class="price">$89 per month (per student)</span> 
                        <?php echo discount_message( $message, $discountKey ); ?>

                    </div>
                    <a href="#signupform" class="btn btn-lg signup smoothScroll">
                         SIGN UP NOW!
                    </a>
                </div>
            </div>
        </div>
    </header>
	
    <!-- Discount Section -->
    <section class="discount">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3>Family Discount!</h3>
                </div>
            </div>
            <div class="row">
                <div class="footer-col col-md-4 text-center">
                    <p>10% off your 2<sup>nd</sup> student</p>
                </div>
                <div class="footer-col col-md-4 text-center">
                    <p>15% off your 3<sup>rd</sup> student</p>
                </div>
                <div class="footer-col col-md-4 text-center">
                    <p>20% off your 4<sup>th</sup> student</p>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>The ultimate Choicelunch package to take lunch off your plate.</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="benefits">
                    <div class="col-lg-2">
                        <p><i class="fa fa-fw fa-arrow-up"></i>
                        </p>
                        <p>Order any regular-sized hot entree, any day
						&nbsp;<a href="javascript:void(0);" tabindex="99" data-toggle="popover" data-trigger="focus" data-placement="bottom" 
						  data-content="Upgrade to Premium plan to include large lunch sizes as well as all cold entrees including salads, sushi and wraps."><i class="info fa fa-2x fa-question-circle"></i></a>
						</p>
                    </div>
                    <div class="col-lg-2">
                        <p><i class="fa fa-fw fa-users"></i>
                        </p>
                        <p>Family discount
						 &nbsp;<a href="javascript:void(0);" tabindex="99" data-toggle="popover" data-trigger="focus" data-placement="bottom" 
						  data-content="2nd child 10%-off, 3rd child 15% off and 4th child 20% off."><i class="info fa fa-2x fa-question-circle"></i></a>
						</p>
                    </div>
                    <div class="col-lg-2">
                        <p><i class="fa fa-fw fa-clock-o"></i>
                        </p>
                        <p>Late orders OK! 
						&nbsp;<a href="javascript:void(0);" tabindex="99" data-toggle="popover" data-trigger="focus" data-placement="bottom" 
						  data-content="No fees for Last Minute or Emergency lunches."><i class="info fa fa-2x fa-question-circle"></i></a>
						</p>
                    </div>
                    <div class="col-lg-2">
                        <p><i class="fa fa-fw fa-tablet"></i>
                        </p>
                        <p>Automated or manual ordering available</p>
                    </div>
                    <div class="col-lg-2">
                        <p><i class="fa fa-fw fa-calendar"></i>
                        </p>
                        <p>Sick Days
						 &nbsp;<a href="javascript:void(0);" tabindex="99" data-toggle="popover" data-trigger="focus" data-placement="bottom" 
						  data-content="We don't want you paying for lunches you don't use so we will credit up to 2 weeks of unused days to your account ($5.00 value each) on your final May payment."><i class="info fa fa-2x fa-question-circle"></i></a>
						</p>
                    </div>
                    <div class="col-lg-2">
                        <p><i class="fa fa-fw fa-times-circle-o"></i>
                        </p>
                        <p>Monthly billing, no committment
						 &nbsp;<a href="javascript:void(0);" tabindex="99" data-toggle="popover" data-trigger="focus" data-placement="bottom" 
						  data-content="9 payments Sept-May (1st of each month).  Cancel anytime (effective the next month)."><i class="info fa fa-2x fa-question-circle"></i></a>
						</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    
    <div id="signupform"></div>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Sign Up Now!</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <form id="contactForm" name="sentMessage" class="wufoo topLabel page" accept-charset="UTF-8" autocomplete="off" enctype="multipart/form-data" method="post" novalidate action="https://choicelunch.wufoo.com/forms/za818n30p6jfr3/#public">
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Name</label>
                                <input id="Field1" name="Field1" type="text" class="form-control" placeholder="Name" required data-validation-required-message="Please enter your name."/>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Email Address</label>
                                <input id="Field2" name="Field2" type="email" spellcheck="false" class="form-control" placeholder="Email Address" required data-validation-required-message="Please enter your email address." />
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Phone Number</label>
                                <input id="Field6" name="Field6" type="text" class="form-control" placeholder="Phone Number" required data-validation-required-message="Please enter your phone number." />
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
						
						
                        <div class="row control-group">
                            <div class="form-group non-floating-label-form-group col-xs-12 controls">
								<br>
                                <!--label style="color:#acb6c0;">Choose Plan</label-->
								<select id="Field8" name="Field8" class="form-control" required data-validation-required-message="Please choose a plan.">
									<option value="-Choose Plan-" selected="selected">-Choose Plan-</option>
									<option value="Standard ($89/mo per student)">Standard - $89/mo per student</option>
									<option value="Premium (incl lrg + cold $99/mo per student)" >Premium - $99/mo per student (includes large portions + all cold entrees)</option>
								</select>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
						
						
						
                        <div class="non-floating-label-form-group row control-group">
                            <div class="checkbox col-xs-12 controls">
								<label class="checkbox">
									<input 
										type="checkbox" 
										name="Field9" 
										data-validation-minchecked-minchecked="1"
										data-validation-minchecked-message="Please choose one from below." 
										value="Im ready to sign-up using my stored credit card"
										/> I'm ready to sign-up using my stored credit card
								</label>
							</div>
							<div class="checkbox col-xs-12 controls">
								<label class="checkbox">
									<input 
										type="checkbox" 
										name="Field9" 
										value="Id like more information"
										/> I'd like more information
								</label>
							</div>
								
                            <p class="help-block text-danger"></p>
                        </div>
						
						
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Message</label>
                                <textarea id="Field4" name="Field4" rows="5" class="form-control" placeholder="Message" data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <input id="saveForm" name="saveForm" class="btn btn-success btn-lg" type="submit" value="Send"/>
                                <input type="hidden" id="idstamp" name="idstamp" value="D1Hrv+a07VvlNQDuGTFWzHscZUleoF8+qvDtd38Cayk=" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Location</h3>
                        <p>569 San Ramon Valley Blvd. <br>Danville, CA 94526<br>
                        <a href="mailto:unlimited@choicelunch.com">unlimited@choicelunch.com</a>
						<!--<br>844-262-7441-->
						</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>On the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="https://www.facebook.com/choicelunch" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="http://www.yelp.com/biz/choicelunch-danville" class="btn-social btn-outline"><i class="fa fa-fw fa-yelp"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>About ChoiceLunch</h3>
                        <p>What makes Choicelunch different than other school lunch programs? Simple. We empower kids to make healthy choices. <a href="//www.choicelunch.com/" target="_blank">Visit Choicelunch</a>.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Choice Foodservices, Inc.
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
	<script>
	  $(function () { $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(); } );
	</script>
    <!--<script src="js/contact_me.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>

    <!-- Smooth Scroll -->
    <script src="js/smoothscroll.js"></script>
        
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-5093214-1', 'auto');
      ga('send', 'pageview');

    </script>
	
	<script>
	$(document).ready(function(){
		$('[data-toggle="popover"]').popover();
        $('body').on('click', function (e) {
            $('[data-toggle="popover"]').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });
	});
	</script>

</body>

</html>